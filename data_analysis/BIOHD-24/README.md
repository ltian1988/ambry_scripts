# [BIOHD-24](https://jira.ambrygen.com/browse/BIOHD-24) Estimate the size of the I/O of pipelines and allocate space for the runs requested to be archived by the Validation Team

## Description
This is a request brought up during the BI analysis for Validations meeting on 9/9/2021. 
Some of the points need further clarification.

- make a list of all files input/output generated with a brief description (one panel, or in general..?)
- Make a list of all the runs the Val team wants to archive, with the number of samples in each run.
- Get a ballpark estimate of the size of all those runs/files. Of course, it will all need to be compressed. 
- Ask IT where we can store that.

## Handling the ticket
The file structure of a run in Nextseq is  

As indicated above, one run folder can contain the input and output for multiple panels.

### Step 1
List all panels, input, intermediate, and output file types and sizes.  
*Note that different runs have different number of samples, might just check the per sample file size and space taken*  

#### NGS Panels 172,176,177
Here we set the run folder as the starting point, and use relative paths for the folders in the following steps.
##### Raw reads
For each panel/project within a run folder, they share the same raw reads in `Data/Intensities/BaseCalls`, depending 
on the number of samples in the same run, the total size of the raw reads varies. Here I use 
`210905_NB500919_0155_AH55H2BGXK` as an example.  
This run has 180 samples in total, according to the `SampleSheet.csv`.  
```shell
(base) [ltian@usav1sphpcp1 210905_NB500919_0155_AH55H2BGXK]$ grep '21_' SampleSheet.csv |wc -l
180
```
*Not the best way to find the total number of samples but good enough to check the samples in 2021.*
```shell
(base) [ltian@usav1sphpcp1 Data]$ pwd
/NGS/Nextseq/210905_NB500919_0155_AH55H2BGXK/Data
(base) [ltian@usav1sphpcp1 Data]$ du -h ./
19G     ./Intensities/BaseCalls/L002
19G     ./Intensities/BaseCalls/L001
19G     ./Intensities/BaseCalls/L003
19G     ./Intensities/BaseCalls/L004
75G     ./Intensities/BaseCalls
1.2G    ./Intensities/L002
1.2G    ./Intensities/L001
1.2G    ./Intensities/L003
1.2G    ./Intensities/L004
79G     ./Intensities
79G     ./
```
The entire `Data/` folder takes up to 80 GB space.  
##### De-multiplex
De-multiplexed reads are placed in the `Unaligned_SGE/` folder, size of this folder is:
```shell
(base) [ltian@usav1sphpcp1 Unaligned_SGE]$ pwd
/NGS/Nextseq/210905_NB500919_0155_AH55H2BGXK/Unaligned_SGE
(base) [ltian@usav1sphpcp1 Unaligned_SGE]$ du -h ./ | tail -n 1
93G     ./
```
Among them, for each project/panel:
```shell
(base) [ltian@usav1sphpcp1 Unaligned_SGE]$ du Project_72_Legacy/ -h | tail -n 1
6.5G    Project_72_Legacy/
(base) [ltian@usav1sphpcp1 Unaligned_SGE]$ du Project_76_CardioV2/ -h | tail -n 1
74G     Project_76_CardioV2/
(base) [ltian@usav1sphpcp1 Unaligned_SGE]$ du Project_77_CancerV5/ -h | tail -n 1
3.8G    Project_77_CancerV5/
```
With the numbers of samples for each of the panels
```shell
(base) [ltian@usav1sphpcp1 Project_72_Legacy]$ ls -d Sample_*|wc -l
15
(base) [ltian@usav1sphpcp1 Project_76_CardioV2]$ ls -d Sample_*|wc -l
157
(base) [ltian@usav1sphpcp1 Project_77_CancerV5]$ ls -d Sample_*|wc -l
8
```
##### All the secondary analysis result including BAM files, SAM files, VCF files, intermediate files, and the final report files
These files are all stored in `Aligned_Panel_*_SGE/`, and a huge scale-up in the space is expected:
```shell
(base) [ltian@usav1sphpcp1 210905_NB500919_0155_AH55H2BGXK]$ du -h Aligned_Panel_72_SGE/ -h |tail -n 1
125G    Aligned_Panel_72_SGE
(base) [ltian@usav1sphpcp1 210905_NB500919_0155_AH55H2BGXK]$ du -h Aligned_Panel_76_SGE/ -h |tail -n 1
1.5T    Aligned_Panel_76_SGE/
(base) [ltian@usav1sphpcp1 210905_NB500919_0155_AH55H2BGXK]$ du -h Aligned_Panel_77_SGE/ -h |tail -n 1
81G     Aligned_Panel_77_SGE/
```
A roughly 20x increase in size is observed, comparing to the de-multiplexed reads.  
##### summary
Therefore, we can estimate that, in this run of 180 samples, the space taken by each sample is approximately  
`79/180 + (6.5+74+3.8+125+1.5*1024+81)/180 = 10.6 GB`

#### Panel 167
Use `/NGS/Nextseq/210906_NB552209_0173_AHNVCFBGXJ` as an example.  
##### Number of samples
```shell
(base) [ltian@usav1sphpcp1 210906_NB552209_0173_AHNVCFBGXJ]$ grep '21_' SampleSheet.csv |wc -l
94
```
##### Size of raw reads before demultiplexing
```shell
(base) [ltian@usav1sphpcp1 210906_NB552209_0173_AHNVCFBGXJ]$ du -h Data/ |tail -n 1
56G     Data/
```
So far it looks similar to the other 3 panels.  
##### De-multiplex
```shell
(base) [ltian@usav1sphpcp1 210906_NB552209_0173_AHNVCFBGXJ]$ du -h Unaligned_SGE/ | tail -n 1
62G     Unaligned_SGE/
```
Excluding those that cannot be mapped
```shell
(base) [ltian@usav1sphpcp1 210906_NB552209_0173_AHNVCFBGXJ]$ du -h Unaligned_SGE/Project_67_RNACancer/ | tail -n 1
58G     Unaligned_SGE/Project_67_RNACancer/
```
##### Secondary analysis ouput and intermediate files
```shell
(base) [ltian@usav1sphpcp1 210906_NB552209_0173_AHNVCFBGXJ]$ du -h Aligned_Panel_67_SGE/ | tail -n 1
111G    Aligned_Panel_67_SGE/
```
##### Summary
`(56+58+111)/94 = 2.4 GB`

#### Panel 8901_Neuropathy
`/NGS/Nextseq/210813_NB552207_0179_AHK2KNBGXJ`  
##### Number of samples
```shell
(base) [ltian@usav1sphpcp1 210813_NB552207_0179_AHK2KNBGXJ]$ grep '21_' SampleSheet.csv |wc -l
15
```
##### Size of raw reads before demultiplexing
```shell
(base) [ltian@usav1sphpcp1 210813_NB552207_0179_AHK2KNBGXJ]$ du -h Data/ |tail -n 1
101G    Data/
```
##### De-multiplex
```shell
(base) [ltian@usav1sphpcp1 210813_NB552207_0179_AHK2KNBGXJ]$ du -h Unaligned_SGE/ | tail -n 1
113G    Unaligned_SGE/
```
Excluding those that cannot be mapped
```shell
(base) [ltian@usav1sphpcp1 210813_NB552207_0179_AHK2KNBGXJ]$ du -h Unaligned_SGE/Project_9801_Neuropathy/ | tail -n 1
107G    Unaligned_SGE/Project_9801_Neuropathy/
```
##### Secondary analysis output and intermediate files
```shell
(base) [ltian@usav1sphpcp1 210813_NB552207_0179_AHK2KNBGXJ]$ du -h Aligned_Panel_9801_SGE/|tail -n 1
1.9T    Aligned_Panel_9801_SGE/
```
##### Summary
`(101+107+1.9*1024)/15 = 143.6 GB`
#### Panel 277
`/NGS/Novaseq/210827_A01310_0182_BHJHKCDRXY`
##### Number of samples
```shell
(base) [ltian@usav1sphpcp1 210827_A01310_0182_BHJHKCDRXY]$ grep '21_' SampleSheet.csv |wc -l
383
```
##### Size of raw reads before demultiplexing
```shell
(base) [ltian@usav1sphpcp1 210827_A01310_0182_BHJHKCDRXY]$ du -h Data/ |tail -n 1
113G    Data/
```
##### De-multiplex
```shell
(base) [ltian@usav1sphpcp1 210827_A01310_0182_BHJHKCDRXY]$ du -h Unaligned_SGE/ | tail -n 1
170G    Unaligned_SGE/
```
Excluding those that cannot be mapped
```shell
(base) [ltian@usav1sphpcp1 210827_A01310_0182_BHJHKCDRXY]$ du -h Unaligned_SGE/Project_277_CancerV5/ | tail -n 1
151G    Unaligned_SGE/Project_277_CancerV5/
```
##### Secondary analysis output and intermediate files
```shell
(base) [ltian@usav1sphpcp1 210827_A01310_0182_BHJHKCDRXY]$ du Aligned_Panel_77_SGE/ -h | tail -n 1
3.1T    Aligned_Panel_77_SGE/
```
##### Summary
`(113+151+3.1*1024)/383 = 9.0 GB`


## Summary table
| Catogory ID | Example Run ID | Number of samples | Raw reads size (GB) | Demultiplexed (GB) | Secondary analysis output (GB) | Avg. space taken by each sample (GB) |
| ----------- | -------------- | ----------------- | -------------- | ------------- | ------------------------- | ------------------------------- |
| 172         |NB500919_0155_AH55H2BGXK|        15         |      79        |     6.5       |            125               |   9.2                           |
| 176         |NB500919_0155_AH55H2BGXK|        157        |       -        |     74        |            1.5T           |        10.7                        |
| 177         |NB500919_0155_AH55H2BGXK|        8          |       -        |     3.8       |            81             |     11.0                        |
| 277         |210827_A01310_0182_BHJHKCDRXY |    383            |      113        |    151        |          3.1T              |9.0                              |
| 167         |210906_NB552209_0173_AHNVCFBGXJ|    94             |      56        |    58         |         111               |2.4                              |
| 9801        |210813_NB552207_0179_AHK2KNBGXJ| 15             |        101     |      107      |             1.9T          |  143.6                           |
