import os
from os import listdir
from os.path import isfile, isdir, join
import sys


def raw_reads_size(run_folder):
	cmd = "du -h {} | tail -n 1".format(join(run_folder, 'Data'))
	print("Total size of raw reads:")
	os.system(cmd)


def total_after_demultiplex(run_folder):
	cmd = "du -h {} | tail -n 1".format(join(run_folder, 'Unaligned_SGE'))
	print("Total size of reads after demultiplexing:")
	os.system(cmd)


