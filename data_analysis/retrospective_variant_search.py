import pandas as pd
import pymysql


def connect_to_db():
    engine = pymysql.connect(
        host="usav1svSQLp70",
        user="bio_user",
        passwd="bio@ambry20",
        db="ngs_panels",
        port=3306,
    )
    conn = engine.cursor()
    return engine, conn


def join_tables():
    bhd = pd.read_csv('Exceptions_from_BHD.csv', sep=',', header=0, index_col=None)
    engine, conn = connect_to_db()
    db = pd.read_sql("select id,symbol,c_variant from reporting_range_var_exception", con=engine, index_col=None)
    res = pd.merge(bhd, db, on=['symbol', 'c_variant'], suffixes=('_l','_r'), how='left')
    return res

