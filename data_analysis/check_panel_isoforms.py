import pymysql
import pandas as pd


def connect_to_db():
    engine = pymysql.connect(
        host="usav1svSQLt70",
        user="bio_user",
        passwd="bio@ambry20",
        db="ngs_panels",
        port=3306,
    )
    conn = engine.cursor()
    return engine, conn


def check_panels():
    engine, conn = connect_to_db()
    sql = (
        "select id, panel_index, sub_panel_name, panel_name, num_of_genes, updated_date "
        "from panel_list where active='active'"
    )
    panels = pd.read_sql(sql, engine, index_col="id")
    for id in panels.index:
        sql = "select count(*) from panel2gene where panel_id={}".format(id)
        conn.execute(sql)
        total_number = conn.fetchone()[0]
        sql = "select count(*) from (select * from panel2gene where panel_id={} group by symbol) t".format(
            id
        )
        conn.execute(sql)
        grouped_number = conn.fetchone()[0]
        panels.loc[id, "total_entries"] = total_number
        panels.loc[id, "genes_only"] = grouped_number
    panels.to_csv("panels.csv", sep="\t")
    return panels
