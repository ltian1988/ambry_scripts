import argparse
import os
import json
import pandas as pd
from collections import defaultdict
from os.path import isfile

def parse_input(cnv_qc_fp):
    """
    This script takes in a cnv qc file such as /NGS/Novaseq/210407_A00148_0148_AH3LVJDRXY/Aligned_Exomes_SGE/Project_298_Exome/Sample_RD_20_120403_W1A/RD_20_120403_W1A.cnv.qc and output PASS or FAIL
    """
    with open(cnv_qc_fp, "r") as f:
        for line in f:
            if line.startswith("#QC"):
                return line.rstrip("\n").split(" ")[1]

def parse_cnv_metrics(cnv_metrics):
    """
    This script parses a cnv metric file such as /NGS/Novaseq/210407_A00148_0148_AH3LVJDRXY/Aligned_Exomes_SGE/Project_298_Exome/Sample_RD_20_122162_W1A/RD_20_122162_W1A.metrics
    """
    with open(cnv_metrics, "r") as f:
        for line in f:
            if not line.startswith("sample"):
                return line.rstrip("\n").split("\t")[2:]

def parse_cnv_number(cnv_bed):
    """
    this function gets the cnv calls from cnv.bed files and return cnv call numbers
    """
    if isfile(cnv_bed):
        bdf = pd.read_csv(cnv_bed, skiprows=5, sep='\t', names=['#Chromosome', 'start', 'end', 'type', 'genes','coverage ratio','copy number', 'method'])
        return [str( len(bdf))]
    else:
        return ['0']

def main(args):
    # Open the output file. This is for per sample
    if not os.path.isdir(args.outdir):
        os.mkdir(args.outdir)
    cnv_qc_summary = open(os.path.join(args.outdir, "sample_cnv_qc_summary.csv"), "w")
    print(",".join(["Sample", "CNV QC", "stdev", "mad", "iqr", "bivar", "cnv_calls"]), file=cnv_qc_summary) #print header

    # Process per sample
    if args.sample_list:
        with open(args.sample_list, "r") as f:
            for line in f:
                sample_id = line.rstrip("\n")
                cnv_status = parse_input(os.path.join(args.run_dir, "Sample_" + sample_id, sample_id + ".cnv.qc"))
                cnv_metrics = parse_cnv_metrics(os.path.join(args.run_dir, "Sample_" + sample_id, sample_id + ".metrics"))
                cnv_numbers = parse_cnv_number(os.path.join(args.run_dir, "Sample_" + sample_id, sample_id + ".cnv.bed"))
                out = [sample_id, cnv_status] + cnv_metrics + cnv_numbers
                print(",".join(out), file=cnv_qc_summary)
    else:
        for dir in os.listdir(args.run_dir):
            if dir.startswith("Sample_") and "NEG" not in dir:
                sample_id = dir.split("Sample_")[1]
                cnv_status = parse_input(os.path.join(args.run_dir, "Sample_" + sample_id, sample_id + ".cnv.qc"))
                cnv_metrics = parse_cnv_metrics(os.path.join(args.run_dir, "Sample_" + sample_id, sample_id + ".metrics"))
                cnv_numbers = parse_cnv_number(os.path.join(args.run_dir, "Sample_" + sample_id, sample_id + ".cnv.bed"))
                out = [sample_id, cnv_status] + cnv_metrics + cnv_numbers
                print(",".join(out), file=cnv_qc_summary)

def parse_args():
    parser = argparse.ArgumentParser(description="This script gets sample cnv qc")
    parser.add_argument("--sample_list", help="optional. path to the sample list. This list is supplied from the val team in the excel template. it is copied and saved as a csv. for example: /mnt/tphung/AGS/20-0250_BuccalSwab/ExomeNovaseq_validation/data/exomenovaseq_sample_list.csv")
    parser.add_argument("--run_dir", required=True, help="path to the run dir. for example: /NGS/Novaseq/210407_A00148_0148_AH3LVJDRXY/Aligned_Exomes_SGE/Project_298_Exome")
    parser.add_argument("--outdir", required=True, help="path to the output directory. for example: /mnt/tphung/AGS/20-0250_BuccalSwab/ExomeNovaseq_validation/output")

    return parser.parse_args()

if __name__ == '__main__':
    main(parse_args())

# (base) [dlin@usav1sphpcp1 Exome_CNV_stats]$ find /NGS/Novaseq/ -mindepth 2 -maxdepth 2 -iname *Exome* | grep -v itis|grep 2104|awk -F"/" '{print $4}'| sort -u > april_run_lst