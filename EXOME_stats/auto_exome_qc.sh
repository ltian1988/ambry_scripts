#!/bin/bash

set -eux

WORK_DIR=$PWD


while getopts p:t: flag
do
    case "${flag}" in
        p) PLATEFORM=${OPTARG};;
        t) TIME_GLOB=${OPTARG};;
    esac
done
# PLATEFORM=Nextseq
# PLATEFORM=Novaseq
# TIME_GLOB=2

mkdir ${PLATEFORM}_${TIME_GLOB}
cd ${PLATEFORM}_${TIME_GLOB}
# # Glob the correct month, 2103 for March 2021, decide Novaseq or Nextseq here
ls -d /NGS/${PLATEFORM}/${TIME_GLOB}*/Aligned_Exomes_SGE/Project_*98_Exome/ |cut -d"/" -f4 > runs

PLATEFORM_DIR=$PWD
foo(){
    local run=$1
    mkdir $run
    cd $run
    # Exome
    python ${WORK_DIR}/get_cnv_qc_DL.py --run_dir /NGS/${PLATEFORM}/${run}/Aligned_Exomes_SGE/Project_*98_Exome --outdir .
    mv sample_cnv_qc_summary.csv ${run}_sample_cnv_qc_summary.csv
    cp ${run}_sample_cnv_qc_summary.csv $PLATEFORM_DIR

    ls /NGS/${PLATEFORM}/${run}/Aligned_Exomes_SGE/Project_*98_Exome/Sample_*/[!NEG]*.coverage.json > json_lst
    python ${WORK_DIR}/read_json.py json_lst
    cp ${run}_cov_stats.xlsx $PLATEFORM_DIR

    # # Cancer Stats
    # ls /NGS/N*seq/${runid}/Aligned_Panel_77_SGE/Project_77_CancerV5/Sample_*/*.coverage.json | grep -v NEG > json_lst
    # /home/dlin/miniconda3/envs/ambry/bin/python3.7 /home/dlin/make_scripts/read_json.py json_lst
    # cp *.xlsx $WORK_DIR

}
for runid in `cat runs`; do foo $runid & done

wait
cd $PLATEFORM_DIR
python3 ${WORK_DIR}/compile_Exome_qc.py
mv 98_CNV_stats.xlsx ${PLATEFORM}_${TIME_GLOB}_98_CNV_stats.xlsx
mv avg_cnv_calls_in_passing_samplesVSMadIQRBivarSum.png ${PLATEFORM}_${TIME_GLOB}_avg_cnv_calls_in_passing_samplesVSMadIQRBivarSum.png
mv failure_rateVSMadIQRBivarSum.png ${PLATEFORM}_${TIME_GLOB}_failure_rateVSMadIQRBivarSum.png
cp *.png $WORK_DIR
cp *98_CNV_stats.xlsx $WORK_DIR