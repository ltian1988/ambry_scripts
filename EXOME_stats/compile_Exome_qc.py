import pandas as pd
import os, sys, glob
import seaborn as sns
import matplotlib.pyplot as plt

"""This script compiles monthly Exome QC"""

cov_glob = glob.glob('./*_cov_stats.xlsx')
cnv_glob = glob.glob('./*_sample_cnv_qc_summary.csv')

def ngs_qc(row):
    if float(row['X10']) < 0.95 or float(row['X20'] < 0.9 or row['mean_coverage'] <50 ):
        return "FAIL"
    else:
        return "PASS"

def merge_df(cnv_glob):
    """This function combines two qc metrics dfs"""
    df_lst = []
    for c in cnv_glob:
        runid = "_".join( os.path.basename(c).split('_')[:4])
    #     print(runid)
        cnv_df = pd.read_csv(c)
        cnv_df['runid'] = runid
    #     print(cnv_df)
        cov_df = pd.read_excel(f'{runid}_cov_stats.xlsx')
        cov_df.drop('coverage_qc', axis=1, inplace=True)
    #     print(cov_df)
        mdf = pd.merge(cnv_df, cov_df, left_on='Sample', right_on='sample')
    #     print(mdf)
        df_lst.append(mdf)
    cdf = pd.concat(df_lst, axis=0)
    cdf['NGS_QC'] = cdf.apply(ngs_qc, axis=1)
    cdf['MadIQRBivarSum'] = cdf['mad']+cdf['iqr']+cdf['bivar']
    cdf = cdf[['runid','Sample','X10', 'X20', 'X50','X100', 'mean_coverage', 'NGS_QC', 'CNV QC','cnv_calls', 'stdev', 'mad', 'iqr', 'bivar', 'MadIQRBivarSum']]
    # print(cdf)
    return cdf
    
def make_regplots(df, x_axis, y_axis):
    sns.regplot(x=x_axis, y=y_axis, data=df, line_kws = {"color":"red"}, ci=95)
    plt.savefig(f"{x_axis}VS{y_axis}")
    plt.clf()


def run():
    cdf = merge_df(cnv_glob)

    passing_cdf = cdf[cdf['CNV QC'] == 'PASS']
    writer = pd.ExcelWriter('98_CNV_stats.xlsx', engine='xlsxwriter')
    cdf.to_excel(writer, sheet_name = 'all_samples' ,index = False)
    dup_cdf = cdf
    gcdf = cdf.groupby('runid').mean()
    dup_cdf['cnv qc bool'] = dup_cdf.apply(lambda x: 1 if x['CNV QC'] == 'PASS' else 0, axis=1)
    # This gets the passing sample number and total sample number
    # gcdf['passing_sample_num'] = passing_cdf.groupby('runid').size().to_frame('counts')
    gcdf['passing_sample_num'] = dup_cdf.groupby('runid')['cnv qc bool'].sum()
    gcdf['sample_num'] = cdf.groupby('runid').size().to_frame('counts')
    # gcdf['avg_cnv_calls_in_passing_samples'] = passing_cdf.groupby('runid')['cnv_calls'].mean().round(2)
    gcdf['failure_rate'] = (1 - (gcdf['passing_sample_num']/gcdf['sample_num']))
    gcdf.reset_index(inplace=True)
    gcdf.rename(columns={'cnv_calls':'avg_cnv_calls'}, inplace=True)
    # gcdf['failure_rate'] = gcdf['failure_rate'].map('{:,.2%}'.format)
    # gcdf['X10'] = gcdf['X10'].map('{:,.2%}'.format)
    # gcdf['X20'] = gcdf['X20'].map('{:,.2%}'.format)
    # gcdf['X50'] = gcdf['X50'].map('{:,.2%}'.format)
    # gcdf['X100'] = gcdf['X100'].map('{:,.2%}'.format)
    gcdf.to_excel(writer, sheet_name = 'run_stats' ,index = False)
    
    noRD_cdf = cdf[~cdf['Sample'].str.contains('RD')]
    # noRD_cdf['MadIQRBivarSum'] = noRD_cdf['mad']+noRD_cdf['iqr']+noRD_cdf['bivar']
    noRD_cdf['MadIQRBivarSum'] = noRD_cdf.apply(lambda x:x['mad']+x['iqr']+x['bivar'], axis=1)
    noRD_passing_cdf = noRD_cdf[noRD_cdf['CNV QC'] == 'PASS']
    noRD_cdf.to_excel(writer, sheet_name = 'all_samples_noRD' ,index = False)
    noRD_gcdf = noRD_cdf.groupby('runid').mean()
    # This gets the passing sample number and total sample number
    noRD_gcdf['passing_sample_num'] = noRD_passing_cdf.groupby('runid').size().to_frame('counts')
    noRD_gcdf['sample_num'] = noRD_cdf.groupby('runid').size().to_frame('counts')
    noRD_gcdf['avg_cnv_calls_in_passing_samples'] = noRD_passing_cdf.groupby('runid')['cnv_calls'].mean().round(2)
    noRD_gcdf['failure_rate'] = (1- (noRD_gcdf['passing_sample_num']/noRD_gcdf['sample_num']))
    noRD_gcdf.reset_index(inplace=True)
    noRD_gcdf.rename(columns={'cnv_calls':'avg_cnv_calls'}, inplace=True)
    # noRD_gcdf['failure_rate'] = noRD_gcdf['failure_rate'].map('{:,.2%}'.format)
    # noRD_gcdf['X10'] = noRD_gcdf['X10'].map('{:,.2%}'.format)
    # noRD_gcdf['X20'] = noRD_gcdf['X20'].map('{:,.2%}'.format)
    # noRD_gcdf['X50'] = noRD_gcdf['X50'].map('{:,.2%}'.format)
    # noRD_gcdf['X100'] = noRD_gcdf['X100'].map('{:,.2%}'.format)
    noRD_gcdf.to_excel(writer, sheet_name = 'run_stats_noRD' ,index = False)

    noRD_passNGSqc_df = noRD_cdf[noRD_cdf['NGS_QC'] == 'PASS']
    noRD_passNGSqc_df['group'] = "total"
    gnRD_passNGSqc_df = noRD_passNGSqc_df.groupby('group').mean()
    gnRD_passNGSqc_df['passing_NGSqc_sample_num'] = noRD_passNGSqc_df.groupby('group').size().to_frame('counts')
    gnRD_passNGSqc_df.reset_index(inplace=True)
    gnRD_passNGSqc_df.to_excel(writer, sheet_name = 'summary' ,index = False)
    writer.save()

    make_regplots(gcdf, "failure_rate", "MadIQRBivarSum")
    make_regplots(gcdf, "avg_cnv_calls_in_passing_samples", "MadIQRBivarSum")




if __name__=="__main__":
    run()