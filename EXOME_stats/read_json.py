import pandas as pd
import json
from pandas.io.json import json_normalize
import sys

"""python read_json.py $coverage.json_lst"""
# switch the metrics for different panel at line 27 and line 43
json_links = sys.argv[1]

def parse_lst(lst):
    json_path_lst = []
    with open(json_links , 'r') as f:
        for line in f:
            runid = line.split('/')[3]
            # print(runid)
            json_path_lst.append(line.strip())
    return json_path_lst, runid

def load_json2df(lst_of_json_path):
    df_lst = []
    for path in lst_of_json_path:
        with open(path, 'r') as f:
            d = json.load(f)
            normdf = json_normalize(d)
            # print(normdf.columns)
            try:
                df = normdf[['sample', 'mean_QS', 'perfect_index', 'yield', 'q30', 'X10', 'X20', 'X50', 'X150', 'X500', 'X1000', 'mean_coverage']] # TN142 downsampled

            except:
                df = normdf[['sample', 'mean_QS', 'perfect_index', 'q30', 'pcr_dup_rate', 'X10', 'X20', 'X50', 'X100', 'mean_coverage']] # Neuropathy, NGS
            # df = normdf[['sample', 'X10', 'X20', 'X50', 'X150', 'X500', 'X1000','yield', 'mean_coverage']] # TN142 downsampled
            
            df_lst.append(df)
    full_df = pd.concat(df_lst, axis = 0)
    full_df.fillna(0, inplace=True)
    return full_df

def status(row):
    meanQS = float(row['mean_QS'])
    prft_idx = float(row['perfect_index'])
    q30 = float(row['q30'])
    x10 = float(row['X10'])
    x20 = float(row['X20'])
    mean_coverage = float(row['mean_coverage'])
    # if meanQS >30 and prft_idx >85 and q30 >75 and x10 >0.85:# this is for ngs panels,there is a slight problem: this does not consider nolocos
    if meanQS >30 and prft_idx >85 and q30 >75 and x10 >0.95 and x20 >0.9 and mean_coverage >50:# This is for HGMD
        return "PASS"
    else:
        return "FAIL"

def run():
    json_path_lst, runid = parse_lst(json_links)
    df = load_json2df(json_path_lst)
    df = df.set_index('sample')
    # df = df.applymap(lambda x: f'{float(x):.2f}') # turn off for TN142 downsampled
    df['coverage_qc'] = df.apply(status, axis = 1) # turn off for TN142 downsampled
    df.to_excel (f'./{runid}_cov_stats.xlsx', sheet_name = 'coverage_stats' ,index = True, header=True)
    print(df)

if __name__=="__main__":
    run()