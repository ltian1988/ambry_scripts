#! /bin/bash
#$ -j y
#$ -cwd
#$ -S /bin/bash
#$ -q low.q
#$ -N ctDNA_test
#$ -m bea
#$ -M ltian@ambrygen.com


source ~/.bashrc
conda activate /mnt/ltian/liquid_biopsy_env

time snakemake \
--snakefile /mnt/ltian/liquid_biopsy_pipeline/Snakefile \
--configfile /mnt/ltian/liquid_biopsy_pipeline/config.yaml \
--config RUN_DIR="/NGS/devNovaseq/220627_A01310_0372_SOMATIC37S" ENV="test" \
--drmaa " -S /bin/bash -v PATH='/mnt/ltian/liquid_biopsy_env/bin:$PATH' {params.qsub} " \
--jobname "{params.name}{jobid}" \
--latency-wait 180 \
--rerun-incomplete \
--nolock \
--keep-going \
--jobs 100 \
-p