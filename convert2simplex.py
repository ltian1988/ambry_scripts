#!/home/zlei/anaconda3/envs/py3/bin/python3
import sys
import os
import gzip
import datetime
import logging
import argparse

log_level = logging.DEBUG
logging.basicConfig(filename=None, format='%(asctime)s [%(levelname)s]: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=log_level)


def main():
	args = get_arguments()

	r1_file = args.input_fastq_R1_file
	input_folder = os.path.dirname(r1_file)
	r1_basename = os.path.basename(r1_file)
	r2_basename = r1_basename.replace("_R1", "_R2")
	r2_file = os.path.join(input_folder, r2_basename)

	out_r1_file = os.path.join(args.output_dir, r1_basename)
	out_r2_file = os.path.join(args.output_dir, r2_basename)

	out_r1_fh = gzip.open(out_r1_file, 'wt')
	out_r2_fh = gzip.open(out_r2_file, 'wt')

	line_num = 0
	with gzip.open(r1_file, 'rt') as fh1, gzip.open(r2_file, 'rt') as fh2:
		for r1_line, r2_line in zip(fh1, fh2):   # or izip_longest
			line_num += 1
			line_pos = line_num % 4
			if line_pos == 1 or line_pos == 3: # read_id or '+' on third line
				out_r1_fh.write(r1_line)
				out_r2_fh.write(r2_line)
			else: #nucleotide sequence or quality score
				out_r1_fh.write(r1_line[:3]+r2_line[0:3]+r1_line[3:])
				out_r2_fh.write(r2_line[5:])
	out_r1_fh.close()
	out_r2_fh.close()

def get_arguments():
	main_description = '''\


Author: Zhengdeng Lei 

This script will convert fastq files from duplex mode to simplex mode.
	'''
	
	epi_log='''
This program requires the python packages:
argparse
logging
	'''	
	help_help = '''\
	show this help message and exit\
	'''
	version_help = '''\
	show the version of this program\
	'''

	input_fastq_R1_file_help = '''\
	input fastq file
	'''

	output_dir_help = '''\
	output directory to store the simplex mode fastq files
	default ./simplex/
	'''

	arg_parser = argparse.ArgumentParser(description=main_description, epilog=epi_log, formatter_class=argparse.RawTextHelpFormatter, add_help=False)
	arg_parser.register('type','bool', lambda s: str(s).lower() in ['true', '1', 't', 'y', 'yes']) # add type keyword to registries
	###############################
	#    1. required arguments    #
	###############################
	required_group = arg_parser.add_argument_group("required arguments")

	###############################
	#    2. optional arguments    #
	###############################
	optional_group = arg_parser.add_argument_group("optional arguments")
	optional_group.add_argument("-o", dest="output_dir", default="simplex", help=output_dir_help)	
	
	###############################
	#    3. positional arguments  #
	###############################
	arg_parser.add_argument('input_fastq_R1_file', default=None, help=input_fastq_R1_file_help)


	optional_group.add_argument("-h", "--help", action="help", help=help_help)
	optional_group.add_argument("-v", "--version", action="version", version="%(prog)s: version 1.0", help=version_help)
	args = arg_parser.parse_args()



	abs_paths = ["input_fastq_R1_file", "output_dir"]
	create_dirs = []
	must_exist_paths = ["input_fastq_R1_file", "output_dir"]
	check_arguments(args, arg_parser, abs_paths, create_dirs, must_exist_paths)
	return args

def check_arguments(args, arg_parser, abs_paths, create_dirs, must_exist_paths):
	for abs_path in abs_paths:
		obj = eval("args." + abs_path)
		if isinstance(obj, list):
			exec("args.{abs_path} = [os.path.abspath(x) for x in args.{abs_path}]".format(abs_path=abs_path))  in globals(), locals()
		else:
			exec("args.{abs_path} = os.path.abspath(args.{abs_path})".format(abs_path=abs_path))  in globals(), locals()

	for create_dir in create_dirs:
		dir_path = eval("args." + create_dir)
		if not os.path.exists(dir_path):
			os.makedirs(dir_path)
			logging.debug("%s is created!\n" % dir_path)

	for must_exist_path in must_exist_paths:
		obj = eval("args." + must_exist_path)
		if isinstance(obj, list):
			for path in obj:
				if not os.path.exists(path):
					arg_parser.print_help()
					logging.error("No file exist as \"%s\" \n" % path )
					sys.exit()					
		else:
			path = obj
			if not os.path.exists(path):
				arg_parser.print_help()
				logging.error("\"%s\" does not exist!!!\n" % path )
				sys.exit()

if __name__ == '__main__':
	main()
