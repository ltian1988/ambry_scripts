#!/home/ltian/anaconda3/bin/python
"""
This script performs a retrospective variant search in all VCF files of selected panels, if not all of them, in the
AVADeposit folder. Note that this folder will be archived periodically, so it can only search within 2-3 years of range.
Command line:
python retrospective_variant_search.py -p {panels, separated by comma} -i {hgvs mutation}

The output is a table of VCF files, named as {mutation}.txt with the the VCF files and the VCF content in that file that
 contains the chromosome+position of the given mutation.
The next step is to look into the output VCF to check the run id and accessions of the found VCF files and remove
1) run ids that are not clinical, e.g. contains FT, SITAO, etc.
2) accessions with non-numeric characters e.g. RD or B
3) variants not the same as the given one.
4) runs that didn't pass QC

1) and 2) are included in this script, so the output {mutation}.txt is cleared from them.
3) and 4) have to be done manually.
3) can be manually checked in the output file whether the ref and alt allele match.
4) needs to be checked on AVA by the accession id to see which run has passed QC.
Changes can be made directly to the output file. After the filtering is done, send it to the stakeholder.
*Note that we are still waiting for the stakeholder's response about how do they want the result looks like, will
probably come back and make changes to that.`

"""
import os
import argparse
import requests
import sys
from os import listdir
from os.path import join, isdir
import pandas as pd


AVA_deposit = '/NGSData/AVADeposit'


def get_parsed_args():
    parser = argparse.ArgumentParser(
        description="Search for past VCF with or without a panel name given a mutation"
    )
    parser.add_argument("-p", dest="panels", help="Panel numbers in 70, 72, 73, 74, 76, 77, 80, 98, 99, separated by comma if there are multiple, e.g. 70,77", type=str, default='')
    parser.add_argument("-i", dest="mutation", help="The mutaiton in HGVS format: 'gene (or transcript accession):c.dot', e.g. 'MYBPC3:c.1224-52G>A'")
    args = parser.parse_args()
    return args


def get_genomic_pos(hgvs):
    server = "https://grch37.rest.ensembl.org"
    ext = "/vep/human/hgvs/{}"
    r = requests.get(server + ext.format(hgvs), headers={"Content-Type": "application/json"})
    if not r.ok:
        r.raise_for_status()
        print("Mutation location not found")
        sys.exit()
    decoded = r.json()[0]
    chrom = 'chr'+str(decoded['seq_region_name'])
    start = decoded['start']
    return chrom, start


def find_accessions(panels, chrom, start):
    output_orig = open("{}:{}.out.orig".format(chrom, start), 'w')
    panels = panels.split(",")
    panel_folders = [d for d in listdir(AVA_deposit) if d.split('_')[0] in panels and isdir(join(AVA_deposit, d))]
    for panel_folder in panel_folders:
        for root, dirs, files in os.walk(join(AVA_deposit, panel_folder)):
            for name in files:
                if name.endswith('.vcf'):
                    vcf = join(root, name)
                    with open(vcf, 'r') as f_vcf:
                        for line in f_vcf.readlines():
                            if line.startswith(chrom):
                                if line.split('\t')[1] == str(start):
                                    output_orig.write(vcf + '\t' + name.split('/')[-1].split('.')[0] + '\t' + line)
                                    break
    output_orig.close()


def filter_output(chrom, start):
    with open("{}:{}.txt".format(chrom, start), 'w') as output_filtered:
        with open("{}:{}.out.orig".format(chrom, start), 'r') as output_orig:
            for line in output_orig.readlines():
                fields = line.split('\t')
                filename = fields[0]
                run_id = filename.split('/')[-2]
                accession = fields[1]
                conditions = ['FT' not in run_id, 'SITAO' not in run_id, 'RD' not in accession, 'B' not in accession]
                if all(conditions):
                    output_filtered.write(line)
    output_filtered = pd.read_csv("{}:{}.txt".format(chrom, start), sep='\t',header=None, index_col=None)
    output_filtered.sort_values([1], inplace=True)
    output_filtered.to_csv("{}:{}.txt".format(chrom, start), sep='\t', header=False, index=False)


if __name__ == '__main__':
    args = get_parsed_args()
    panels = args.panels
    mutation = args.mutation
    chrom, start = get_genomic_pos(mutation)
    find_accessions(panels, chrom, start)
    filter_output(chrom, start)






