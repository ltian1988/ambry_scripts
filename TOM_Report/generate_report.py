import pandas as pd
import argparse


def get_parsed_args():
    parser = argparse.ArgumentParser(
        description="Generate Monthly TOM report"
    )
    parser.add_argument("-i", dest="raw_report_file", help="The file name of raw report downloaded from TOM, in CSV format", type=str, default='')
    parser.add_argument("-c", dest="cap_file",
                        help="The capitalized projectd list from Bing, saved in CSV format, with headers of 'Project Name', and 'R&DID'.")
    parser.add_argument("-o", dest="output_file",
                        help="The output BI_Capitalization_month_year file, in CSV format.")
    args = parser.parse_args()
    return args


def check_total_hours(raw_report):
	total_hours = raw_report.groupby('User')['Hours Logged'].agg('sum')
	sorted_total_hours = total_hours.sort_values()
	print("Check if there are abnormally low-hour and high-hour entries.")
	print(sorted_total_hours)
	print("Ignore the next steps and contact the employees with abnormally low or high hours.")


def fill_empty_rd_id(raw_report, cap_df):
	for i in raw_report.index:
		if pd.isnull(raw_report.loc[i,'R&D ID']):
			proj_report = raw_report.loc[i,'Project Name']
			if len(cap_df[cap_df['Project Name']==proj_report]['R&DID'])>0:
				rd_id_cap = cap_df[cap_df['Project Name']==proj_report]['R&DID'].values[0]
				raw_report.loc[i,'R&D ID'] = rd_id_cap
	return raw_report


def get_BI_capitalization_table(raw_report, cap_df, output_file):
	BI_cap = pd.DataFrame()
	unique_rd_id = list(cap_df['R&DID'].unique())
	for rd_id in unique_rd_id:
		sliced_report = raw_report[(raw_report['R&D ID']==rd_id) & (~raw_report['Project Name'].str.contains('[Rr]esearch'))]
		BI_cap = BI_cap.append(sliced_report, ignore_index=True)
	BI_cap['corrected Total Hours'] = BI_cap.apply(lambda row: sum(row[7:]), axis=1)
	BI_cap.drop(columns=['User Id', 'Category Name', 'Project Id', 'Hours Logged'], inplace=True)
	BI_cap.insert(loc=3, column='Total Hours', value=BI_cap['corrected Total Hours'])
	BI_cap.drop(columns=['corrected Total Hours'], inplace=True)
	BI_cap.to_csv(output_file, sep=',', index=False)


if __name__ == '__main__':
	args = get_parsed_args()
	cap_file = args.cap_file
	raw_report_file = args.raw_report_file
	output_file = args.output_file
	cap_df = pd.read_csv(cap_file, sep=',', header=0, index_col=None)
	raw_report = pd.read_csv(raw_report_file, sep=',', header=0, index_col=None)
	raw_report['R&D ID'] = raw_report['R&D ID'].str.strip()
	raw_report = fill_empty_rd_id(raw_report, cap_df)
	check_total_hours(raw_report)
	get_BI_capitalization_table(raw_report, cap_df, output_file)



