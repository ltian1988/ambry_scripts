The script `generate_report.py` is used to generate the monthly report of capitalizable hours entered into TOM, based on the generic TOM report, capitalization projects provided by the financial team, and the capitalizable projects and code used in TOM.  

```
usage: generate_report.py [-h] [-i RAW_REPORT_FILE] [-c CAP_FILE]
                          [-o OUTPUT_FILE]

Generate Monthly TOM report

optional arguments:
  -h, --help          show this help message and exit
  -i RAW_REPORT_FILE  The file name of raw report downloaded from TOM, in CSV
                      format
  -c CAP_FILE         The capitalized projectd list from Bing, saved in CSV       
                      format, with headers of 'Project Name', and 'R&DID'.        
  -o OUTPUT_FILE      The output BI_Capitalization_month_year file, in CSV        
                      format.
```

## Dependency
This script requires the installation of [`pandas`](https://pandas.pydata.org/). 