#!/bin/bash
#$ -j y
#$ -cwd
#$ -S /bin/bash
#$ -pe mpi 4
#$ -q low.q
#$ -M ltian@ambrygen.com
#$ -m beas


function showtime {
local T=$1
local D=$((T/60/60/24))
local H=$((T/60/60%24))
local M=$((T/60%60))
local S=$((T%60))
printf 'Running time: '
(( $D > 0 )) && printf '%d days ' $D
(( $H > 0 )) && printf '%d hours ' $H
(( $M > 0 )) && printf '%d minutes ' $M
(( $D > 0 || $H > 0 || $M > 0 )) && printf 'and '
printf '%d seconds\n' $S
}



date
start=`date +%s`
echo -e "===================\n"

/home/ltian/anaconda3/bin/python /mnt/ltian/ambry_scripts/query_spliceai_precalc_files.py /mnt/ltian/RD_21_00011_K1.RD_21_00011_J1.combined.sorted.vcf.gz /mnt/ltian/RD_21_00011_K1.RD_21_00011_J1.combined.sorted.query_files_mpi4.vcf 4

echo -e "\n==================="
date
end=`date +%s`
t=$((end-start))
showtime $t