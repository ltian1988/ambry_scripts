#! /bin/bash
#$ -j y
#$ -cwd
#$ -S /bin/bash
#$ -q test.q
#$ -N ctDNA_test


BASE_DIR=/NGS/Novaseq
RUNID=220310_A01310_0288_CTDNATESTS
SAMPLE_SHEET=/NGS/Novaseq/220310_A01310_0288_CTDNATESTS/Data/Intensities/BaseCalls/samplesheet.csv

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/swu/bin/gcc-7.1.0/lib64:/mnt/swu/bin/zlib-1.2.11/lib
bcl2fastq=/mnt/swu/request/rna-seq/rna-seq_v2_novaseq/bcl2fastq_v2.20/bin/bcl2fastq
date
$bcl2fastq --runfolder-dir ${BASE_DIR}/${RUNID} -o ${BASE_DIR}/${RUNID}/Unaligned_RAW/ --sample-sheet $SAMPLE_SHEET --no-lane-splitting --barcode-mismatches 0


echo "demultiplex completed"
date

html_file=`ls /NGS/Novaseq/220310_A01310_0288_CTDNATESTS/Unaligned_RAW/Reports/html/*/all/all/all/laneBarcode.html`

/home/lteng/miniconda3/bin/python /mnt/tkitapci/lteng/commonutilities/get_sequencing_qc_v3.py  --html $html_file --output ${RUNID}_QC_Metrics.csv

echo "QC done"
date
