#!/bin/bash
#$ -j y
#$ -cwd
#$ -S /bin/bash
#$ -pe mpi 4
#$ -q low.q
#$ -M ltian@ambrygen.com
#$ -m a

# set -eux
WORK_DIR=$PWD

rsync -agovP --exclude={'Aligned_*','Unaligned_SGE*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs'} \
            /NGS/Novaseq/210722_A00137_0256_AHH527DRXY/ /NGS/valNovaseq/210722_A00137_0256_PIPE301ABC
cd /NGS/valNovaseq/210722_A00137_0256_PIPE301ABC
sed -i 's/210722_A00137_0256_AHH527DRXY/210722_A00137_0256_PIPE301ABC/g' *.xml
cd Data/Intensities/BaseCalls
mkdir PIPE301
ll -art|grep 'clinical' | awk '{print $9}'| xargs -I{} mv {} PIPE301/


rsync -agovP --exclude={'Aligned_*','Unaligned_SGE*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs'} \
            /NGS/Novaseq/210718_A01252_0129_AHH5FTDRXY/ /NGS/valNovaseq/210718_A01252_0129_PIPE301ABC
cd /NGS/valNovaseq/210718_A01252_0129_PIPE301ABC
sed -i 's/210718_A01252_0129_AHH5FTDRXY/210718_A01252_0129_PIPE301ABC/g' *.xml
cd Data/Intensities/BaseCalls
mkdir PIPE301
ll -art|grep 'clinical' | awk '{print $9}'| xargs -I{} mv {} PIPE301/

# # Change runid in RunInfo.xml and RunParameters.xml in the run folder
# # sed -i 's/210207_A01252_0021_BH2FGCDRXY/210207_A01252_0021_MASSARAYFT/g' Run*.xml

