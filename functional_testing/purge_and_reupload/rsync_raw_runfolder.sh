#!/bin/bash
#$ -j y
#$ -cwd
#$ -S /bin/bash
#$ -pe mpi 4
#$ -q low.q
#$ -M dlin@ambrygen.com
#$ -m a

# set -eux
WORK_DIR=$PWD

# for runid in `cat runs`
#     do
#         RUN_LOCATION=`ls -d /NGS/N*seq/${runid}/`
#         SEQUENCER=`echo ${RUN_LOCATION}|awk -F"/" '{print $3}'`
#         FT_RUN_NAME=`echo ${runid}|sed 's/.\{10\}$//' | awk '{print $1"NGS3101TST"}'`
#         FT_RUN_LOCATION=/NGS/val${SEQUENCER}/${FT_RUN_NAME}
#         echo copying $RUN_LOCATION $FT_RUN_LOCATION
#         mkdir -p ${FT_RUN_LOCATION}
#         rsync -avz --exclude={'Aligned_*','Unaligned_SGE*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images', 'jobs'} \
#                 ${RUN_LOCATION} ${FT_RUN_LOCATION}
        
        # cd ${FT_RUN_LOCATION}
        # sed -i "s/${runid}/${FT_RUN_NAME}/g" Run*.xml
        # mkdir backup_NGS3101
        # # This dosn't work right now as rsync does not preserve ownership
        # # ls -alrt|grep clinical| awk '{print $9}'| xargs -I{} mv {} backup_RNA140FT
        # cd Data/Intensities/BaseCalls/
        # mkdir backup_NGS3101
        # # ls -alrt|grep clinical| awk '{print $9}'| xargs -I{} mv {} backup_RNA140FT
        # cd ${WORK_DIR}
#     done

# # We want to save everything about this run
# rsync -agovP /NGS/Nextseq/210527_NB552205_0167_AHGC27BGXH/ /NGS/rdNextseq/210527_NB552205_0167_AHGC27BGXH

rsync -agovP --exclude={'Aligned_*','Unaligned_SGE*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs'} \
            /NGS/Restores/tphung/Ticket_34750/210409_A01252_0061_AH3GLNDRXY/ /NGS/rdNovaseq/210409_A01252_0061_AH3GLNDRXY
# # Change runid in RunInfo.xml and RunParameters.xml in the run folder
# # sed -i 's/210207_A01252_0021_BH2FGCDRXY/210207_A01252_0021_MASSARAYFT/g' Run*.xml


# # Another Parallel
# run_rsync() {
#     # e.g. copies /main/files/blah to /main/filesTest/blah
#     rsync -av "$1" "/main/filesTest/${1}"
# }
# export -f run_rsync
# parallel -j5 run_rsync ::: /main/files/*

# Check if all the runs are ok
# (base) [dlin@usav1sphpcp1 server_upload_issue]$ for i in `ls /NGS/valNextseq/210206_NS500379_0620_NGS310TEST/Aligned_Panel_77_SGE/Project_*/Sample_*/*-err.out`; do echo $i; cat $i| grep Automatic_Upload.pm; done|grep -B1 "Automatic_Upload.pm"|grep "210206_NS500379_0620_NGS310TEST"| wc -l