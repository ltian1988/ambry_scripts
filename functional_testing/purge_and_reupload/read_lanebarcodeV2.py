from bs4 import BeautifulSoup
import pandas as pd
import sys, glob

"""This will work when there are multiple panels like 72, 76, 77 all in the same run
    python read_lanebarcodeV2.py $runid
    """

runid = sys.argv[1]
html = glob.glob(f'/NGS/*seq/{runid}/Unaligned_SGE/Reports/html/*/all/all/all/laneBarcode.html')[0]

def status_no_noloco(row):
    """ this function looks at the cells in a row so need to use axis = 1 during apply"""
    if row['% Perfectbarcode'] > 85 and row['% >= Q30bases'] > 75 and row['Mean QualityScore'] > 30:
        return 'PASS'
    return 'FAIL'

def status_w_noloco(row):
    """ this function looks at the cells in a row so need to use axis = 1 during apply"""
    if row['% Perfectbarcode'] > 85 and row['% >= Q30bases'] > 75 and row['Mean QualityScore'] > 30 and row['Noloco'] <= 10:
        return 'PASS'
    return 'FAIL'

df_lst = pd.read_html(html)
# there are 3 df in df_lst
# for df in df_lst:
#     print(df)
# print(df_lst[2])

df = df_lst[2]
df.columns = df.columns.droplevel(0)
df.drop(['#', 'Project', 'Barcode sequence', r'% of thelane', r'% One mismatchbarcode', r'% PFClusters'], inplace=True, axis=1)
# df = df.loc[:,~df.columns.duplicated()] # keep all the rows and remove duplicate columns, duplicated() returns boolean array, it is flipped by "~"
df = df.groupby('Sample').sum()
# df.drop(['unknown'], inplace=True, axis=0)
df = df/4 # to get the avg num/ lane, there are 4 lanes in nextseq
df['Clusters'] = df['Clusters']*4 # there is no need to get avg cluster
df['Yield (Mbases)'] = df['Yield (Mbases)']*4
df = df.applymap(lambda x: float(f'{x:.1f}')) # formats the numbers to 1 digit after desimal point
df = df.reset_index()
df = df[~(df.Sample == 'unknown')]
# print(df.head())


# Need to put a session for noloco in the future
try:
    # Find noloc specific to panel 77
    noloco_glob = glob.glob(f'/NGS/*seq/{runid}//Aligned_Panel_7?_SGE/Project_*/Report_Files/summary.noloco.txt')
    # print(noloco_glob)
    if len(noloco_glob) > 0:
        print('found noloco!')
        ndf_lst = []
        for n in noloco_glob:
            ndf = pd.read_csv(n, header=0)
            ndf_lst.append(ndf)
        cndf = pd.concat(ndf_lst, axis=0)
        cndf['Sample'] = cndf['SAMPLE'].apply( lambda x: x.split('/')[-1].split('.')[0])
        cndf.drop('SAMPLE', inplace=True, axis=1)
        cndf.rename(columns={'NOLOCO_Exons':'Noloco'}, inplace=True)

        df2 = pd.merge(df, cndf, on='Sample', how='left')
        print(df2.head())
        df2['Status'] = df2.apply(status_w_noloco, axis=1)
        writer = pd.ExcelWriter(f'{runid}_seqQC.xlsx', engine='xlsxwriter')
        df2.to_excel(writer, sheet_name = f'{runid}' ,index = False)
        writer.save()
    else:
        df['Status'] = df.apply(status_no_noloco, axis=1)
        writer = pd.ExcelWriter(f'{runid}_seqQC.xlsx', engine='xlsxwriter')
        df.to_excel(writer, sheet_name = f'{runid}' ,index = False)
        writer.save()

except Exception as e:
    print(f'error {e}')
    raise
    print('noloco not found')
    df['Status'] = df.apply(status_no_noloco, axis=1)
    print(df)
    writer = pd.ExcelWriter(f'{runid}_seqQC.xlsx', engine='xlsxwriter')
    df.to_excel(writer, sheet_name = f'{runid}' ,index = True)
    writer.save()
    