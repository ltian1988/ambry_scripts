
# This works on single sample
#Author T. Hamdi Kitapci
# run that you'd like to del from
runID="210904_NB552205_0205_AHNYTGBGXJ"
# sample=$2
#sample "20-XXXXXX"
# sample=21_276254

panel="67" #Panel
deleteAccessionFile="delete_accession_list_"$runID"_"$panel".txt"
purgeAndUploadCommandsFile="purge_and_upload_commands_"$runID"_"$panel".bash"
database="usav1svhaps70" #Change this to p70 if it is for production

# # If deleting all accession
# echo $sample | tr "_" "-" > $deleteAccessionFile
cat /NGS/Nextseq/"$runID"/Data/Intensities/BaseCalls/samplesheet.csv |grep -A1000 "Sample_ID" |awk 'BEGIN{FS=","}{if (NR>1) print $1}'| tr "_" "-" > $deleteAccessionFile #Add the accessions to be deleted here
# /mnt/tkitapci/daily_operations/helper_scripts/getdelcmd_noRunID_noPanelID.pl $deleteAccessionFile $database >$purgeAndUploadCommandsFile
# cat /mnt/dlin/functional_testing/past_FT/RNA140CLINBOT251/delete_upload_67_4/header >$purgeAndUploadCommandsFile
/mnt/tkitapci/daily_operations/helper_scripts/getdelcmd.pl $runID "1"$panel $deleteAccessionFile $database >>$purgeAndUploadCommandsFile

# If uploading from a new run
newRunID="210904_NB552207_0189_RNAV150FT1"
# reportFiles=`ls -d /NGS/Nextseq/"$newRunID"/Aligned_Panel_"$panel"_SGE/*/Report_Files/`
# reportFiles=`ls -d /NGS/Novaseq/"$newRunID"/Aligned_Panel_"$panel"_SGE/*/Report_Files/`
reportFiles=`ls -d /NGS/valNextseq/"$newRunID"/Aligned_Panel_"$panel"_SGE/*/Report_Files/`

echo reportFiles folder is $reportFiles
#Reupload accession from this run
for i in `cat $deleteAccessionFile|grep -v "#"|tr "-" "_"`
do
 
    echo "date" >>$purgeAndUploadCommandsFile
    # Use this for Panel_Pipelien upload
	# echo python /mnt/clinical/bin/Scripts/Panel_Pipeline/call_auto_upload_deldup_single.py $reportFiles $i $database yes no >>$purgeAndUploadCommandsFile
	# echo perl /mnt/tkitapci/Pipeline_Local/automatic_upload_samplebased_forFTClinbot251/autoupload_ngs_single.pl $reportFiles $i $database yes no >>$purgeAndUploadCommandsFile
	
	# #Use this for RNA upload
	# #uppath="/home/Share/automatic_upload_samplebased"
	# uppath="/mnt/tkitapci/Pipeline_Local/automatic_upload_samplebased_forFTClinbot251"
	echo python /mnt/clinical/bin/rnaseq_pipeline_v1.5.0_for_test/call_auto_upload_single.py $reportFiles $i $database yes no $uppath >>$purgeAndUploadCommandsFile

done

echo Delete and re-upload commands are in: $purgeAndUploadCommandsFile
