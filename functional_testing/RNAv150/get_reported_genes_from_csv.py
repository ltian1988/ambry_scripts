from os import listdir
from os.path import isfile, join
import sys


def get_reported_genes_from_csv(csv_file_folder):
    reported_genes = []
    csv_files = [
        join(csv_file_folder, f)
        for f in listdir(csv_file_folder)
        if f.endswith(".all.csv")
    ]
    with open("reported_genes_all_csv.txt", "w") as output_handler:
        for each in csv_files:
            with open(each, "r") as f:
                lines = [i.strip().split(",") for i in f.readlines()[1:]]
            for line in lines:
                gene = line[6]
                if gene not in reported_genes:
                    reported_genes.append(gene)
                    output_handler.write(gene + "\n")
    return reported_genes


def compare_gene_lists(reported_genes, reportable_gene_file):
    with open(reportable_gene_file, "r") as f:
        reportable_genes = f.read().strip().split(", ")
    unreported_genes = list(
        set(reportable_genes) - set(reportable_genes).intersection(set(reported_genes))
    )
    print("Number of unreported genes: {}".format(len(unreported_genes)))
    if len(unreported_genes) > 0:
        with open("unreported_genes_all_csv.txt", "w") as f:
            for i in unreported_genes:
                f.write(i + "\n")


def something(input1, input2):
    """
	Description:
	@param input1:
	@param input2:
	@return:
	"""


if __name__ == "__main__":
    csv_file_folder = sys.argv[1]
    reportable_gene_file = sys.argv[2]
    reported_genes = get_reported_genes_from_csv(csv_file_folder)
    compare_gene_lists(reported_genes, reportable_gene_file)
