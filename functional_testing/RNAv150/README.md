# RNAv1.5.0 functional testing

## step 1: select runs with good quality or go to AVA3 to pick one
`find /NGS/Nextseq -maxdepth 2 -mindepth 2 -type d -name Aligned_Panel_67_SGE`  
210904_NB552207_0189_AHT3GNBGXJ

## step 2: get the QC metrics report
`python read_lanebarcodeV2.py 210904_NB552207_0189_AHT3GNBGXJ`

## step 3: Rsync
`qsub` the rsync script by changing the folder to the correct panel, change the run_id, 
rsync it to a test folder with the suffix of the folder name still 10-digit long.

## step 4: Clean-up
When Rsync is done, go to the folder in `valXXXX` folder that the samples just rsynced to.  
### Change the run_id
Change the original run_id to the newly created one in the following XML files:
```shell
RunInfo.xml
RunParameters.xml
# And this is specifically for RNA
RunCompletionStatus.xml
```
with this bash command
```shell
sed -i 's/$old_runid/$new_runid/g' *.xml
sed -i 's/210904_NB552207_0189_AHT3GNBGXJ/210904_NB552207_0189_RNAV150FT1/g' *.xml
```
### Backup the BaseCall folder
```shell
cd Data/Intensities/BaseCalls
mkdir RNAv150_FT
# Depending on the ownership of the folders and files
# ll -art|grep clinical| awk '{print $9}'| xargs -I{} echo mv {} RNAv150_FT/
# Exclude the L00* folders if the ownership has been changed during rsync
ll -art|grep -v 'L00[1234]|RNAv150_FT' | awk '{print $9}'| xargs -I{} echo mv {} RNAv150_FT/
# The previous command confirms the folders and files to be backed-up
# And now we remove the echo
ll -art|grep -v 'L00[1234]|RNAv150_FT' | awk '{print $9}'| xargs -I{} mv {} RNAv150_FT/
```
### Remove/Backup the sample sheet so it run will not picked up by valbot until ready
```shell
mv SampleSheet.csv SampleSheet.csv.RNAV150FT1
```
## Step 5: Drop samplesheet when valbot is ready
```shell
(base) [dlin@usav1sphpcp1 210904_NB552207_0189_RNAV150FT1]$ pwd
/NGS/valNextseq/210904_NB552207_0189_RNAV150FT1
(base) [dlin@usav1sphpcp1 210904_NB552207_0189_RNAV150FT1]$ mv RTAComplete.txt RTAComplete_test.txt

(base) [dlin@usav1sphpcp1 BaseCalls]$ pwd
/NGS/valNextseq/210904_NB552207_0189_RNAV150FT1/Data/Intensities/BaseCalls
(base) [dlin@usav1sphpcp1 BaseCalls]$ cp RNAv150_FT/samplesheet.csv .
# valbot picked up the run
(base) [dlin@usav1sphpcp1 BaseCalls]$ ls
L001  L002  L003  L004  nohup_bcl2fq_sge.out  RNAv150_FT  samplesheet.csv  samplesheet_original.csv  sample_sheet_warnings.txt
```