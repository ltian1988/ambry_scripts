from os import listdir
from os.path import join
import pandas as pd
import dask.dataframe as dd


def investigate_excluded_genes(report_file_folder, unreported_genes):
	df = dd.read_csv(report_file_folder + "/21_*.with_control.csv", sep=',')
	for gene in unreported_genes:
		sub = df[df['Gene']==gene]
		sub['Coverage'] = sub.apply(lambda x: float(x['#Reads_supporting_splicing'])/x['#Total_reads'], axis=1)
		filter = sub[(sub['Normalized_reads_supporting_splicing(%)']>5) & (sub['FDR']<=0.05) & (sub['Coverage']>0.05)]
		print("{} variant(s) in the gene {} found passed the QC.".format(len(filter.head().index), gene))