import os
import shutil
import argparse

def get_parsed_args():
    parser = argparse.ArgumentParser(
        description="Search for past VCF with or without a panel name given a mutation"
    )
    parser.add_argument("-o", dest="original", help="Original project folder", type=str)
    parser.add_argument("-v", dest="validation", help="The new project folder", type=str)
    parser.add_argument("-d", dest="dest", help="Destination folder", type=str)
    args = parser.parse_args()
    return args


def copy_ava(folder):
