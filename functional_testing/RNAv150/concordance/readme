# dev test checklist here: https://jira.ambrygen.com/browse/PIPE-229
# 1. Splicing results for all 91 CancerV5 genes with sufficient coverage are output to the ava.txt file and successfully uploaded to AVA3. 
		# - If any genes have no splicing results, it's sufficient to load one sample in IGV and check that it is missing due to insufficient coverage
		# - Test upload to t70 database - this is independent of IT updates made to ngs_panels.panel2gene & ngs_panels.panel_list.num_of_genes for panel 167
		# - Check that new genes are visible and masked in AVA3 case page 
# 2. Concordance of the splicing events for the 18 reportable genes between original run and dev test run to make sure nothing changed
# 3. Make sure the % exons over 50x is the same as the original run 


# testing a run 210829_NB501613_0425_AHTGHVBGXJ in /NGS/devNextseq/210829_NB501613_0425_ACASSRNA15 Sitao tested a single sample in /NGS/devNextseq/210829_NB501613_0425_SITAORNARN/
	## make job files
	cd /NGS/devNextseq/210829_NB501613_0425_ACASSRNA15
	mkdir jobs
	rsync /NGS/devNextseq/210829_NB501613_0425_SITAORNARN/R* ./
	for dir in /NGS/Nextseq/210829_NB501613_0425_AHTGHVBGXJ/Aligned_Panel_67_SGE/Project_67_RNACancer/Sample_*; do acc=${dir#*Sample_}; jobfile=./jobs/pipeline_210829_NB501613_0425_ACASSRNA15_67_RNACancer_${acc}.job; head -9 /NGS/devNextseq/210829_NB501613_0425_SITAORNARN/jobs/test.job > $jobfile; echo /mnt/clinical/bin/rnaseq_pipeline_v1.5.0_for_test/rna_splicing_pipeline.pl -i /NGS/devNextseq/210829_NB501613_0425_ACASSRNA15/Aligned_Panel_67_SGE/Project_67_RNACancer -s ${acc} -d db70_test -u /home/Share/automatic_upload_samplebased_for_hgmdnext_test -e acass >> $jobfile; done

	## as clinical
	cd jobs
	for file in jobs/pipeline_210829_NB501613_0425_ACASSRNA15_67_RNACancer_*; do qsub $file; done


# check for all 91 genes in *all.csv files
	cd /mnt/acass/rna/pipeline/v1.5.0/dev_test
	for file in /NGS/devNextseq/210829_NB501613_0425_ACASSRNA15/Aligned_Panel_67_SGE//Project_67_RNACancer/Report_Files/*all.csv; do cut -d ',' -f 7 $file | tail -n+2; done | sort | uniq > unique_genes_csv.txt
	awk 'NR==FNR {a[$1]; next} !($1 in a) {print $1}' unique_genes_csv.txt ../91genes_on_website.txt
		# CPA1
		# EGFR
		# GREM1
		# HOXB13
		# PHOX2B
		# SPINK1

# check for all 91 genes in *ava.txt files
	for file in /NGS/devNextseq/210829_NB501613_0425_ACASSRNA15/Aligned_Panel_67_SGE//Project_67_RNACancer/Report_Files/*ava.txt; do grep -v "^#" $file | cut -d ' ' -f 1; done | sort | uniq > unique_genes_ava.txt
	awk 'NR==FNR {a[$1]; next} !($1 in a) {print $1}' unique_genes_ava.txt ../91genes_on_website.txt
		# AIP
		# ALK
		# CDK4
		# CDKN1B
		# CFTR
		# CPA1
		# EGFR
		# EPCAM
		# GREM1
		# HOXB13
		# MET
		# PDGFRA
		# PHOX2B
		# PRSS1
		# RPS20
		# SDHB
		# SDHD
		# SPINK1
		# TERT
		# TP53

# concordance of 18 genes splicing events
	## get list of 18 reportable genes
	cut -d ' ' -f 4 /mnt/clinical/bin/rnaseq_pipeline/ref/allgene.txt | sort | uniq > reportable_18genes.txt

	## get just the 18 genes results from the ava files 
	for file in /NGSData/RNAResult/210829_NB501613_0425_AHTGHVBGXJ/*ava.txt; do test=${file/AHTGHVBGXJ/ACASSRNA15}; out=${file##*/}; awk -F ' ' 'NR==FNR {a[$1]; next} $1 in a {print $0}' reportable_18genes.txt $file > ava_splicing_events_clinical/$out; awk -F ' ' 'NR==FNR {a[$1]; next} $1 in a {print $0}' reportable_18genes.txt $test > ava_splicing_events_test/$out; done

	## concordance check for 18 genes results
	for file in ava_splicing_events_clinical/*; do test=${file/clinical/test}; discordant=ava_splicing_events_discordant/${file##*/} nclinical=$(wc -l < $file); ntest=$(wc -l < $test); awk -F ' ' 'NR == FNR {a[$1":"$2":"$3]; next} !($1":"$2":"$3 in a) {print $0}' $test $file > $discordant; ndiscordant=$(wc -l < $discordant); echo ${file##*/}","$nclinical","$ntest","$ndiscordant; done > ava_concordance_check.csv

	## check that discordant calls are due to different FDR 
	for file in ava_splicing_events_discordant/*; do if [ $(wc -l < $file) != 0 ]; then acc=${file##*/}; acc=${acc%.ava.txt}; clinicalcsv=/NGSData/RNAResult/210829_NB501613_0425_AHTGHVBGXJ/${acc}.splicing_events.with_control.csv; testcsv=/NGSData/RNAResult/210829_NB501613_0425_ACASSRNA15/${acc}.splicing_events.with_control.csv; awk 'NR==FNR {a[$1":"$2":"$3]; next} $7":"$8":"$9 in a {print $0}' FS=' ' $file FS=',' $clinicalcsv > csv_with_control_discordant_clinical/${acc}.splicing_events.with_control.csv; awk 'NR==FNR {a[$1":"$2":"$3]; next} $7":"$8":"$9 in a {print $0}' FS=' ' $file FS=',' $testcsv > csv_with_control_discordant_test/${acc}.splicing_events.with_control.csv; fi; done


	for file in csv_with_control_discordant_clinical/*; do test=${file/clinical/test}; acc=${file##*/}; acc=${acc%.splic*}; awk -F ',' -v OFS=',' -v acc="$acc" 'NR==FNR {a[$1":"$2":"$3":"$4":"$5":"$6":"$7":"$8":"$9":"$10":"$11":"$12":"$13":"$14":"$15":"$16":"$17]=$18; next} $1":"$2":"$3":"$4":"$5":"$6":"$7":"$8":"$9":"$10":"$11":"$12":"$13":"$14":"$15":"$16":"$17 in a {print acc, $0, a[$1":"$2":"$3":"$4":"$5":"$6":"$7":"$8":"$9":"$10":"$11":"$12":"$13":"$14":"$15":"$16":"$17]}' $file $test; done > discordant_calls_compare_fdr.csv
		# -> the minimum FDR in the test run is 0.053 which is > 0.05

# concordance of % exons over 50x
	for file in /NGS/Nextseq/210829_NB501613_0425_AHTGHVBGXJ/Aligned_Panel_67_SGE/Project_67_RNACancer/Sample*/*.coverage.final; do test=${file/Nextseq/devNextseq}; test=${test/210829_NB501613_0425_AHTGHVBGXJ/210829_NB501613_0425_ACASSRNA15}; diff $file $test; done
	# -> no standard output / all files are the same
