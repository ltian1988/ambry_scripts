#!/bin/bash

set -eux

WORK_DIR=$PWD

# bash clean_up.sh >>clean_up.out 2>&1
# Clean val RNA run records in automatic_pipeline for valbot pickups
for run in `cat runs`
    do
        echo cleaning ${run}

        mkdir ${run}
        cd ${run}

        # Generate patient_status cleaning job 
        # /home/dlin/miniconda3/envs/giab/bin/python2.7 /mnt/dlin/functional_testing/RNA140CLINBOT251/delete_acc_67_3/getDeleteCommands_forEntireRun.py ${run} usav1svhaps70 > delete_cmd_for_${run}_usav1svhaps70.job
        bash /mnt/dlin/functional_testing/RNA140CLINBOT251/delete_upload_67_4/generate_purge_and_upload_commands_samples.sh ${run}

        # # Clean the NGSData folder, no need ot clean if it is re-upload, only need to clean these if we are re-runing 
        # mv /NGSData/CoverageReportDeposit/${run} /NGSData/CoverageReportDeposit/${run}.bak
        # mv /NGSData/BamDeposit/67_RNACancer/${run} /NGSData/BamDeposit/67_RNACancer/${run}.bak
        # mv /NGSData/RNACoverageDeposit/${run} /NGSData/RNACoverageDeposit/${run}.bak
        # mv /NGSData/RNAResult/${run} /NGSData/RNAResult/${run}.bak

        # Backup upload logs
        mkdir upload_logs_backup
        # mv /mnt/tkitapci/Pipeline_Local/automatic_upload_samplebased_forFTClinbot251/logs/*${run}* upload_logs_backup/
        # Hamdi's backup method for safety, create 
        for i in `ls -d /mnt/tkitapci/Pipeline_Local/automatic_upload_samplebased_forFTClinbot251/logs/*${run}*`;do echo mv $i upload_logs_backup;done > ${run}_upload_log_backup_cmd.bash

        # Backup db
        echo "select * from automatic_upload where runid=\"${run}\";" > ${run}_AU_backup.sql
        mysql -h usav1svhaps70 -u reporter -p ngs_panels --password="123" < ${run}_AU_backup.sql > ${run}_AU_backup.sql.out
        echo "DELETE from automatic_upload where runid=\"${run}\";" > ${run}_AU_delete.sql
        mysql -h usav1svhaps70 -u reporter -p ngs_panels --password="123" < ${run}_AU_delete.sql > ${run}_AU_delete.sql.out
        mysql -h usav1svhaps70 -u reporter -p ngs_panels --password="123" < ${run}_AU_backup.sql > ${run}_AU_backup.sql.checkdel.out

        echo "select * from patient_status where path like \"%${run}%\";" > ${run}_PS_backup.sql
        mysql -h usav1svhaps70 -u reporter -p ngs_panels --password="123" < ${run}_PS_backup.sql > ${run}_PS_backup.sql.out

        # # Only need to do this when re-run
        # echo "select * from qc_metrics_rna where run_id like \"${run}\";" > ${run}_QC_backup.sql
        # mysql -h usav1svhaps70 -u reporter -p ngs_panels --password="123" < ${run}_QC_backup.sql > ${run}_QC_backup.sql.out
        # echo "DELETE from qc_metrics_rna where run_id like \"${run}\";" > ${run}_QC_delete.sql
        # mysql -h usav1svhaps70 -u reporter -p ngs_panels --password="123" < ${run}_QC_delete.sql > ${run}_QC_delete.sql.out
        # mysql -h usav1svhaps70 -u reporter -p ngs_panels --password="123" < ${run}_QC_backup.sql > ${run}_QC_backup.sql.checkdel.out

        # # Clean up the run folder
        # cd /NGS/valNextseq/${run}

        # mkdir -p backup_RNA140FT
        # # Just in case that there is already a folder with same name in the backup_RNAFT folder
        # mv backup_RNA140FT backup_RNA140FT.bak1
        # mkdir -p backup_RNA140FT

        # ls -alrt|grep clinical|grep -v "\.\."| awk '{print $9}'| xargs -I{} mv {} backup_RNA140FT/

        # cd Data/Intensities/BaseCalls/
        # mkdir -p backup_RNA140FT
        # mv backup_RNA140FT backup_RNA140FT.bak1
        # mkdir -p backup_RNA140FT
        # ls -alrt|grep clinical |grep -v "\.\."| awk '{print $9}'| xargs -I{} mv {} backup_RNA140FT/
        chmod -R 777 .
        cd ${WORK_DIR}
    done


# # Backup, Purge and upload one sample
# # bash clean_up.sh >>clean_up.out 2>&1
# for line in `cat run_samples`
#     do
#         run=`echo ${line}|cut -d"," -f1`
#         sample=`echo ${line}|cut -d"," -f2`
#         echo "select * from automatic_upload where runid=\"${run}\" and accession like \"${sample}\";" > ${run}_${sample}_AU_backup.sql
#         mysql -h usav1svhaps70 -u reporter -p ngs_panels --password="123" < ${run}_${sample}_AU_backup.sql > ${run}_${sample}_AU_backup.sql.out
#         echo "DELETE from automatic_upload where runid=\"${run}\";" > ${run}_${sample}_AU_delete.sql
#         mysql -h usav1svhaps70 -u reporter -p ngs_panels --password="123" < ${run}_${sample}_AU_delete.sql > ${run}_AU_delete.sql.out
#         # Check deletion
#         mysql -h usav1svhaps70 -u reporter -p ngs_panels --password="123" < ${run}_${sample}_AU_backup.sql > ${run}_${sample}_AU_backup.sql.delete_chekc.out

#         echo "select * from patient_status where path like \"%${run}%\" and accession_id like \"${sample}\";" > ${run}_${sample}_PS_backup.sql
#         mysql -h usav1svhaps70 -u reporter -p ngs_panels --password="123" < ${run}_${sample}_PS_backup.sql > ${run}_${sample}_PS_backup.sql.out

#         bash /mnt/dlin/functional_testing/RNA140CLINBOT251/upload_1/generate_purge_and_upload_commands_RNA.sh ${run} ${sample}
#     done

