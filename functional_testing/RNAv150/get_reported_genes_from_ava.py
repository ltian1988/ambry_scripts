from os import listdir
from os.path import join, isfile
import sys


def list_reported_genes(ava_file_folder):
    reported_genes = []
    ava_files = [
        join(ava_file_folder, f)
        for f in listdir(ava_file_folder)
        if f.endswith(".ava.txt")
    ]
    with open("reported_genes.txt", "w") as output_handler:
        for each in ava_files:
            with open(each, "r") as f:
                lines = [
                    i.strip().split(" ") for i in f.readlines() if not i.startswith("#")
                ]
            for line in lines:
                gene = line[0]
                if gene not in reported_genes:
                    reported_genes.append(gene)
                    output_handler.write(gene + "\n")
    return reported_genes


def compare_gene_lists(reported_genes, reportable_gene_file):
    with open(reportable_gene_file, "r") as f:
        reportable_genes = f.read().strip().split(", ")
    unreported_genes = list(
        set(reportable_genes) - set(reportable_genes).intersection(set(reported_genes))
    )
    print("Number of unreported genes: {}".format(len(unreported_genes)))
    if len(unreported_genes) > 0:
        with open("unreported_genes.txt", "w") as f:
            for i in unreported_genes:
                f.write(i + "\n")


if __name__ == "__main__":
    ava_file_folder = sys.argv[1]
    reportable_gene_file = sys.argv[2]
    reported_genes = list_reported_genes(ava_file_folder)
    compare_gene_lists(reported_genes, reportable_gene_file)
