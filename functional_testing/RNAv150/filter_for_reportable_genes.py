"""
python filter_for_reportable_genes.py source_ava_folder output_ava_folder gene_list.txt
"""
import os
from os import listdir
from os.path import isfile, join
import sys


def get_reportable_gene_list(gene_list_file):
	with open(gene_list_file, 'r') as f:
		reportable_genes = [i.strip() for i in f.readlines()]
	return reportable_genes


def filter_ava(source_ava_folder, output_ava_folder, reportable_genes):
	original_ava_files = [f for f in listdir(source_ava_folder) if isfile(join(source_ava_folder, f))]
	for each in original_ava_files:
		output_handler = open(join(output_ava_folder, each), 'w')
		with open(join(source_ava_folder, each), 'r') as src_handler:
			lines = [i.strip() for i in src_handler.readlines()]
		for line in lines:
			if line.startswith('#'):
				output_handler.write(line + '\n')
			else:
				gene = line.split(' ')[0]
				if gene in reportable_genes:
					output_handler.write(line + '\n')
		output_handler.close()


if __name__ == '__main__':
	source_ava_folder = sys.argv[1]
	output_ava_folder = sys.argv[2]
	gene_list_file = sys.argv[3]
	reportable_genes = get_reportable_gene_list(gene_list_file)
	filter_ava(source_ava_folder, output_ava_folder, reportable_genes)
