#!/bin/bash
#$ -j y
#$ -cwd
#$ -S /bin/bash
#$ -pe mpi 4
#$ -q low.q
#$ -M ltian@ambrygen.com
#$ -m a

# set -eux
WORK_DIR=$PWD


rsync -agovP --exclude={'Aligned_*','Unaligned_SGE*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs'} \
            /NGS/Novaseq/210814_A01437_0062_BHHH52DRXY/ /NGS/valNovaseq/210814_A01437_0062_Ticket9290
# # Change runid in RunInfo.xml and RunParameters.xml in the run folder
# # sed -i 's/210207_A01252_0021_BH2FGCDRXY/210207_A01252_0021_MASSARAYFT/g' Run*.xml
