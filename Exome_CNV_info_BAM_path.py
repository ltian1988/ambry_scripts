import os
import sys
import requests
import json
from os import listdir
from os.path import isfile, isdir, join


def get_genomic_pos(hgvs):
	server = "https://grch37.rest.ensembl.org"
	ext = "/vep/human/hgvs/{}"
	r = requests.get(server + ext.format(hgvs), headers={"Content-Type": "application/json"})
	if not r.ok:
		r.raise_for_status()
		sys.exit()
	decoded = r.json()[0]
	chrom = decoded['seq_region_name']
	start = decoded['start']
	allele_string = decoded['allele_string']
	print("chr{}:{}:{}".format(chrom, start, allele_string))


def find_paths(family_id):
	"""
	This function looks for run_id, and then paths to the family VCF as well as the path to each of the sample BAM files
	"""
	vcf_folder = "/NGS/Exome3/NGS-results/Fam_{}".format(family_id)
	vcf_path = vcf_folder + f"/Fam_{family_id}.vcf"
	sample_dirs = [join(vcf_folder, d) for d in listdir(vcf_folder) if
	               isdir(join(vcf_folder, d)) and d.startswith("Sample_") and not d.endswith('orig')]
	print("scp ltian@hpc1:{}/Fam_{}.vcf C:\\Users\\ltian\\Desktop\\Exome_CNV\\{}\\".format(vcf_folder, family_id,family_id))
	bam_paths = []
	for d in sample_dirs:
		coverage_json = [join(d, f) for f in listdir(d) if f.endswith(".coverage.json")][0]
		with open(coverage_json, 'r') as f:
			info = json.load(f)
		# vcf_path = info['vcf_path'].replace('.final','')
		bam_paths.append(info['path'])
	bam_paths_string = ", ".join(bam_paths)
	print(f"VCF file path: {vcf_path}")
	print("--Downloading command--")
	print("scp ltian@hpc1:{} C:\\Users\\ltian\\Desktop\\Exome_CNV\\{}\\".format(vcf_path, family_id))
	print(f"BAM file locations: {bam_paths_string}")
	print("--Downloading command--")
	for each in bam_paths:
		print("scp ltian@hpc1:{} C:\\Users\\ltian\\Desktop\\Exome_CNV\\{}\\".format(each, family_id))
		print("scp ltian@hpc1:{}.bai C:\\Users\\ltian\\Desktop\\Exome_CNV\\{}\\".format(each, family_id))

if __name__ == '__main__':
	hgvs = sys.argv[1]
	family_id = sys.argv[2]
	try:
		get_genomic_pos(hgvs)
	except:
		print("Location not found")
	find_paths(family_id)
