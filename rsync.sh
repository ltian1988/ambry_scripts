#!/bin/bash
#$ -j y
#$ -cwd
#$ -S /bin/bash
#$ -pe mpi 1
#$ -q low.q
#$ -M ltian@ambrygen.com
#$ -m bea

# set -eux
WORK_DIR=$PWD

# mkdir /NGS/valNextseq/211115_NB552205_0238_RNAV2FT001
# rsync -agvP --exclude={'Aligned_*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs', 'Unaligned_SGE'} \
#             /NGS/devNextseq/RNAV1/211115_NB552205_0238_AHT3NFBGXK/ /NGS/valNextseq/211115_NB552205_0238_RNAV2FT001
# cd /NGS/valNextseq/211115_NB552205_0238_RNAV2FT001
# touch .clinbot_ignore_this
# sed -i 's/211115_NB552205_0238_AHT3NFBGXK/211115_NB552205_0238_RNAV2FT001/g' Run*.xml

# mkdir /NGS/valNovaseq/220209_A01310_0275_RNAV2FT001
# rsync -agvP --exclude={'Aligned_*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs', 'Unaligned_SGE'} \
#             /NGS/devNovaSeq/RNAV2/220209_A01310_0275_BHWY5CDRXY/ /NGS/valNovaseq/220209_A01310_0275_RNAV2FT001
# cd /NGS/valNovaseq/220209_A01310_0275_RNAV2FT001
# touch .clinbot_ignore_this
# sed -i 's/220209_A01310_0275_BHWY5CDRXY/220209_A01310_0275_RNAV2FT001/g' Run*.xml

# mkdir /NGS/valNovaseq/220530_A01427_0314_RNAV2FT001
# rsync -agvP --exclude={'Aligned_*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs', 'Unaligned_SGE'} \
#             /NGS/devNovaSeq/RNAV2/220530_A01427_0314_BH3W3YDRX2/ /NGS/valNovaseq/220530_A01427_0314_RNAV2FT001
# cd /NGS/valNovaseq/220530_A01427_0314_RNAV2FT001
# touch .clinbot_ignore_this
# sed -i 's/220530_A01427_0314_BH3W3YDRX2/220530_A01427_0314_RNAV2FT001/g' Run*.xml

# mkdir /NGS/valNovaseq/220530_A01427_0314_RNAV2FT002
# rsync -agvP --exclude={'Aligned_*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs', 'Unaligned_SGE'} \
#             /NGS/devNovaSeq/RNAV2/220530_A01427_0314_BH3W3YDRX2/ /NGS/valNovaseq/220530_A01427_0314_RNAV2FT002
# cd /NGS/valNovaseq/220530_A01427_0314_RNAV2FT002
# touch .clinbot_ignore_this
# sed -i 's/220530_A01427_0314_BH3W3YDRX2/220530_A01427_0314_RNAV2FT002/g' Run*.xml

mkdir /NGS/valNovaseq/220602_A01427_0315_RNAV2FT001
rsync -agvP --exclude={'Aligned_*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs', 'Unaligned_SGE'} \
            /NGS/devNovaSeq/RNAV2/220602_A01427_0315_BH3TLFDRX2/ /NGS/valNovaseq/220602_A01427_0315_RNAV2FT001
cd /NGS/valNovaseq/220602_A01427_0315_RNAV2FT001
touch .clinbot_ignore_this
sed -i 's/220602_A01427_0315_BH3TLFDRX2/220602_A01427_0315_RNAV2FT001/g' Run*.xml