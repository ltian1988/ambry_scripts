#!/bin/bash
#$ -j y
#$ -cwd
#$ -S /bin/bash
#$ -pe mpi 4
#$ -q low.q
#$ -M ltian@ambrygen.com
#$ -m a

# set -eux
WORK_DIR=$PWD

mkdir /NGS/valNovaseq/220310_A01310_0288_UATARIA447
rsync -agvP --exclude={'Unaligned_SGE*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs'} \
            /NGS/Novaseq/220310_A01310_0288_AH37TFDRX2/ /NGS/valNovaseq/220310_A01310_0288_UATARIA447
cd /NGS/valNovaseq/220310_A01310_0288_UATARIA447
touch .clinbot_ignore_this
sed -i 's/220310_A01310_0288_AH37TFDRX2/220310_A01310_0288_UATARIA447/g' Run*.xml
# cd Data/Intensities/BaseCalls
# mkdir Tian_20220304
# ll -art|grep 'clinical' | awk '{print $9}'| xargs -I{} echo mv {} Tian_20220304/

# mkdir /NGS/valNextseq/220113_A00148_0221_ACLINBOTFT
# rsync -agvP --exclude={'Aligned_*','Unaligned_SGE*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs'} \
#             /NGS/Novaseq/220113_A00148_0221_BHTWJMDRXY/ /NGS/valNovaseq/220113_A00148_0221_ACLINBOTFT
# cd /NGS/valNextseq/220113_A00148_0221_ACLINBOTFT
# sed -i 's/220113_A00148_0221_BHTWJMDRXY/220113_A00148_0221_ACLINBOTFT/g' Run*.xml
# cd Data/Intensities/BaseCalls
# mkdir Tian_20220304
# ll -art|grep 'clinical' | awk '{print $9}'| xargs -I{} echo mv {} Tian_20220304/


# mkdir /NGS/valNextseq/220204_A01252_0328_ACLINBOTFT
# rsync -agvP --exclude={'Aligned_*','Unaligned_SGE*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs'} \
#             /NGS/Novaseq/220204_A01252_0328_BHW7WYDRXY/ /NGS/valNovaseq/220204_A01252_0328_ACLINBOTFT
# cd /NGS/valNextseq/220204_A01252_0328_ACLINBOTFT
# sed -i 's/220204_A01252_0328_BHW7WYDRXY/220204_A01252_0328_ACLINBOTFT/g' Run*.xml
# touch .clinbot_ignore_this
# cd Data/Intensities/BaseCalls
# mkdir Tian_20220304
# ll -art|grep 'clinical' | awk '{print $9}'| xargs -I{} echo mv {} Tian_20220304/
# cp /NGSData/BIDSCAFastqDeposit/Archive/220204_A01252_0328_BHW7WYDRXY.csv /NGS/ScipherNovaseq/BIDSCAFastqDeposit/220204_A01252_0328_ACLINBOTFT.csv

# # Another Parallel
# run_rsync() {
#     # e.g. copies /main/files/blah to /main/filesTest/blah
#     rsync -av "$1" "/main/filesTest/${1}"
# }
# export -f run_rsync
# parallel -j5 run_rsync ::: /main/files/*

# Check if all the runs are ok
# (base) [dlin@usav1sphpcp1 server_upload_issue]$ for i in `ls /NGS/valNextseq/210206_NS500379_0620_NGS310TEST/Aligned_Panel_77_SGE/Project_*/Sample_*/*-err.out`; do echo $i; cat $i| grep Automatic_Upload.pm; done|grep -B1 "Automatic_Upload.pm"|grep "210206_NS500379_0620_NGS310TEST"| wc -l