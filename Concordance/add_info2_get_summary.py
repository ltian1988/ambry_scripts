import pandas as pd
import os, sys, glob

from pandas.core.algorithms import mode

""" This script needs ''concordance_*.xlsx' and 'var_db_v3.xlsx'. It will compile final concordance results"""

def add_info2(result_df, dic_df):
    mdf = pd.merge(result_df, dic_df, how='left', on=['Gene', 'Isoform', 'C_variant'])
    non_homo_var_df = mdf[~mdf['info2'].str.contains('homopolymer')]
    # print(mdf)
    return mdf, non_homo_var_df

def get_info2Added_df(globbed_concord_xlsx_lst, db_df):
    """This function takes globbed_concord_df_lst and db_df to append 'info2' column with nucleotide feature
        it output a _info2Added.xlsx and a non_homo.xlsx"""
    summary_cond_df_lst = []
    summary_ucli_df_lst = []
    summary_uval_df_lst = []
    for g in globbed_concord_xlsx_lst:
        file_name = os.path.basename(g).split('.')[0]
        
        writer = pd.ExcelWriter(f'{file_name}_info2Added.xlsx', engine='xlsxwriter')
        non_homo_writer = pd.ExcelWriter(f'{file_name}_non_homo.xlsx', engine='xlsxwriter')
        
        cond_df = pd.read_excel( g, sheet_name='concordant')
        ucli_df = pd.read_excel( g, sheet_name='unique_clinical')
        uval_df = pd.read_excel( g, sheet_name='unique_validation')
        print('adding_info2')
        
        # no need to add varinfo if it is concordant
        cond_df.to_excel( writer, sheet_name='concordant', index=False)
        summary_cond_df_lst.append( cond_df)
        
        ucli_df, ucli_non_homo_df = add_info2(ucli_df, db_df)
        if ucli_df is not None :
            ucli_df.to_excel( writer, sheet_name='unique_clinical', index=False)
            summary_ucli_df_lst.append( ucli_df)
        if ucli_non_homo_df is not None:
            ucli_non_homo_df.to_excel( non_homo_writer, sheet_name='unique_clinical', index=False)
            
        uval_df, uval_non_homo_df = add_info2(uval_df, db_df)
        if uval_df is not None :
            uval_df.to_excel( writer, sheet_name='unique_validation', index=False)
            summary_uval_df_lst.append(uval_df)
        if uval_non_homo_df is not None:
            uval_non_homo_df.to_excel( non_homo_writer, sheet_name='unique_validation', index=False)
        
        writer.save()
        non_homo_writer.save()
        
        # summary_cond_df = pd.concat( summary_cond_df_lst, axis=0)
        # summary_ucli_df = pd.concat( summary_ucli_df_lst, axis=0)
        # summary_uval_df = pd.concat( summary_uval_df_lst, axis=0)
        # summary_writer = pd.ExcelWriter(f'concordance_summary.xlsx', engine='xlsxwriter')

        # summary_cond_df.to_excel( summary_writer, sheet_name='concordant', index=False)
        # summary_ucli_df.to_excel( summary_writer, sheet_name='unique_clinical', index=False)
        # summary_uval_df.to_excel( summary_writer, sheet_name='unique_validation', index=False)
        # summary_writer.save()
        
def extract_homo(df):
    df_var_cnt = len(df)
    non_homo_var_df = df[~df['info2'].str.contains('homopolymer')]
    non_homo_var_cnt = len(non_homo_var_df)
    return df_var_cnt, non_homo_var_df, non_homo_var_cnt

def parse_var_sum_df(non_homo_df):
    if len(non_homo_df) == 0:
        return non_homo_df
    # print(non_homo_df.head())
    # To handle the homo calls where het ratio = 1 but fill with "-"
    non_homo_df['Het ratio'] = pd.to_numeric(non_homo_df['Het ratio'], errors='coerce')
    non_homo_df = non_homo_df.fillna(1)
    # After het ratio is handled then we can take the mean
    df = non_homo_df.groupby(['Gene', 'Isoform', 'C_variant']).mean()
    # need to choose one of the approch 
    df['var_count'] = non_homo_df.groupby(['Gene', 'Isoform', 'C_variant']).size()
    # sdf = non_homo_df.groupby(['Gene', 'Isoform', 'C_variant']).size().to_frame('var_counts')
    # df = pd.merge( df, sdf, on=['Gene', 'Isoform', 'C_variant'], how='inner')
    
    df['info'] = non_homo_df.groupby(['Gene', 'Isoform', 'C_variant'])['info'].apply(lambda x: ','.join(x))
    df['info2'] = non_homo_df.groupby(['Gene', 'Isoform', 'C_variant'])['info2'].apply(lambda x: ';'.join(x))
    df['var_info'] = df['info'].str.split(';').str[0] + ';' + df['info2'].str.split(';').str[0]
    df['var_info'] = df['var_info'].str.replace('Low Het,', '')
    df.insert(0, 'category',df['var_info'].str.split(',').str[0] )
    df.drop(['info', 'info2', 'Ref read count', 'Alt read count'], inplace=True, axis=1)
    df.rename(columns={ 'Het ratio': 'avg Het ratio'})
    return df

def get_final_df(info2Added_glob):
    for g in info2Added_glob:
        file_name = os.path.basename(g).split('.')[0]
        print(file_name)
        writer = pd.ExcelWriter(f'{file_name}_final.xlsx', engine='xlsxwriter')
        
        cond_df = pd.read_excel( g, sheet_name='concordant')
        ucli_df = pd.read_excel( g, sheet_name='unique_clinical')
        uval_df = pd.read_excel( g, sheet_name='unique_validation')
        
        print('compiling tables')

        gcon_df = cond_df.groupby('Validation Accession').count()
        # gcon_df.reset_index()
        # gcon_df.to_csv(f'{file_name}_acc.csv', index=True)
        sample_num = len(gcon_df)
        print(f'sample_num: {sample_num}')

        cond_var_cnt = len(cond_df)
        ucli_var_cnt, ucli_non_homo_df, ucli_non_homo_cnt = extract_homo( ucli_df)
        uval_var_cnt, uval_non_homo_df, uval_non_homo_cnt = extract_homo( uval_df)
        total_cli_var_cnt = cond_var_cnt + ucli_non_homo_cnt
        total_val_val_cnt = cond_var_cnt + uval_non_homo_cnt
        cond_rate = cond_var_cnt/ total_cli_var_cnt
        
        cond_rate_df = pd.DataFrame( [[sample_num, cond_var_cnt, total_cli_var_cnt, total_val_val_cnt, ucli_non_homo_cnt, uval_non_homo_cnt, cond_rate]],
                                    columns=['# samples', '# concordant variants', '# variants called in clinical', '# variants called in validation',
                                                    '# unique var in clinical', '# unique var in validation', '%% concordance'])
        
        ucli_sum_df = parse_var_sum_df( ucli_non_homo_df)
        uval_sum_df = parse_var_sum_df( uval_non_homo_df)
        
        cond_df.to_excel( writer, sheet_name='concordant', index=False)
        ucli_df.to_excel( writer, sheet_name='unique_cli', index=False)
        uval_df.to_excel( writer, sheet_name='unique_val', index=False)
        cond_rate_df.to_excel( writer, sheet_name='summary', index=False)
        ucli_sum_df.to_excel( writer, sheet_name='cli_var_summary')
        uval_sum_df.to_excel( writer, sheet_name='val_var_summary')
        writer.save()


def run():
    initial_glob = glob.glob('./*_concordance.xlsx')
    db_xlsx = 'var_db_v3.xlsx'
    print(f'concordance initial result: {initial_glob}, database: {db_xlsx}')
    db_df = pd.read_excel(db_xlsx)
    get_info2Added_df(initial_glob, db_df)
    info2Added_df_glob = glob.glob('./*info2Added.xlsx')
    get_final_df(info2Added_df_glob)

    # xlsx = '/mnt/dlin/tickets/CancerNovaseqVal2_7715/concordance/201230_A00148_0121_BHYHY7DRXX_concordance_non_homo.xlsx'
    # df = pd.read_excel(xlsx)
    # ucli_sum_df = parse_var_sum_df(df)
    # print(ucli_sum_df)

    
if __name__=="__main__":
    run()