from bs4 import BeautifulSoup
import requests
import re
import urllib.request
import sys
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import os
import json

with open('/mnt/ltian/ambry_scripts/secret.json', 'r') as f:
    CONFIG = json.load(f)

""" Don't forget to update login and password in the script!!! This version will take a excel file and fill in the info, return a excel file 'test.xlsx'
      *Working in progress: automate the read from concordant.csv, unique_clinical.csv, and unique_validation.csv 
      to generate concordance_result.csv with 3 sheets filled with info """

session = requests.Session()
payload = {'email_address':CONFIG['username'],
          'password':CONFIG['password']}
s = session.post("http://ava3.ambrygen.com/authentication/login", data=payload)

classification_category = r'(New|new|NEW|Unclassified|unclassified|UNCLASSIFIED|Mutation|mutation|MUTATION|VLP|VUS|VLB|Silent_DNR|silent_DNR|SILENT_DNR|Poly|poly|POLY|Auto_filter|auto_filter|AUTO_FILTER|Category_6|category_6|CATEGORY_6)'

def get_record_v2(row):
    """ This funciton takes a row and returns classification and record info
            and save a seq.svg file"""

    gene = row['Gene'].strip()
    iso = row['Isoform'].strip()
    var = row['C_variant'].strip()
    print(gene, iso, var)
    
    var_changed = var.replace('+', '%2B')
    s = session.get(f'http://ava3.ambrygen.com/alterations/alterationshow?alteration={iso}+{var_changed}')
    soup = BeautifulSoup(s.text, 'html.parser')
    try:
        classification_div =  soup.findAll("div", {"class":"ui-widget-content vertical-middle"})[0]
        classification = re.findall( classification_category, str(classification_div))[0]
        print(classification)
    except Exception as e:
        print(e, gene, iso, var, 'error!')
        print(soup.findAll("div", {"class":"ui-widget-content vertical-middle"}))
        raise

    if classification == 'New' or classification == 'new' or classification == 'NEW':
        img_id = soup.findAll("img", {"id":"nucleotide_image"})[0]
        img = re.findall( r'<img id="nucleotide_image" src="(\/alterations.*)">', str(img_id))[0]
        img_url = "http://ava3.ambrygen.com/" + img
        img_name_var = var.replace('+', '').replace('>', '').replace('*', '')
        os.makedirs('./svg', exist_ok=True)
        urllib.request.urlretrieve(img_url, f"./svg/{gene}_{img_name_var}.svg")
        return f'{gene} {iso} {var} {classification}, complete'

    record_div = soup.findAll("div", {"class":"previous_records_section margin"})
    # print(record_div)
    record_inter = ', '.join([str(r) for r in record_div])
    record_long = re.findall( r'<span id="previous_records_count">((.*\n)+).*<\/p>', str(record_inter))

    if len(record_long) > 0:
        record = record_long[0][0].replace(' ', '').replace('\n', ' ').replace('</span>', ' ').strip()
        record = re.sub(r'Withinthose[\d\,]+record\(s\)\,', '', record)
        record_num = re.findall( r'([\d,\,]+\s+record\(s\))', record.split('.')[0])[0].replace(' record(s)', 'observations')
        record_info = record.split('.')[1]

        img_id = soup.findAll("img", {"id":"nucleotide_image"})[0]
        img = re.findall( r'<img id="nucleotide_image" src="(\/alterations.*)">', str(img_id))[0]
        img_url = "http://ava3.ambrygen.com/" + img
        img_name_var = var.replace('+', '').replace('>', '').replace('*', '')
        os.makedirs('./svg', exist_ok=True)
        urllib.request.urlretrieve(img_url, f"./svg/{gene}_{img_name_var}.svg")
        return f'{classification}, {record_num}, {record_info}'.replace('None, ', '')
    elif len(record_long) == 0:
        return f'{gene} {iso} {var} record not found, complete'
        
# with concurrent.futures.ProcessPoolExecutor(max_workers=10) as executor:
#     results = executor.map( get_record_v2, biglst)




def run():
    
    writer = pd.ExcelWriter('test.xlsx', engine='xlsxwriter')
    concord_df = pd.read_csv('./concordant.csv')
    concord_df.drop(['Ref read count', 'Alt read count', 'Qscore', 'Het ratio'], inplace=True, axis=1)
    concord_df.to_excel(writer, sheet_name = 'concordant' ,index = False, header=True)

    cli_df = pd.read_csv('./unique_clinical.csv')
    if len(cli_df) > 0:
        cli_df['Het ratio'] = pd.to_numeric(cli_df['Het ratio'], errors='coerce')
        cli_df['Qscore'] = pd.to_numeric(cli_df['Qscore'], errors='coerce')
        cli_df = cli_df.fillna(1)

    val_df = pd.read_csv('./unique_validation.csv')
    if len(val_df) > 0:
        val_df['Het ratio'] = pd.to_numeric(val_df['Het ratio'], errors='coerce')
        val_df['Qscore'] = pd.to_numeric(val_df['Qscore'], errors='coerce')
        val_df = val_df.fillna(1)

    if (len(cli_df) + len(val_df)) >0:
        cdf = pd.concat([cli_df, val_df], axis=0)
        print(cdf)
        gcdf = cdf.groupby(['Gene','Isoform','C_variant']).mean()
        print(gcdf)
        gcdf.reset_index(inplace=True)
        print(gcdf)
        gcdf['info'] = gcdf.apply(get_record_v2, axis = 1)
        print(gcdf)
        gcdf.drop([ 'Ref read count', 'Alt read count', 'Qscore', 'Het ratio'], inplace=True, axis=1, errors='ignore')
        print(gcdf)
    if 'gcdf' in locals():
        cli_df = pd.merge(cli_df, gcdf, how='left', on=['Gene','Isoform','C_variant'])
        cli_df.sort_values( by = ['Gene', 'C_variant'], inplace=True)


        val_df = pd.merge(val_df, gcdf, how='left', on=['Gene','Isoform','C_variant'])
        val_df.sort_values( by = ['Gene', 'C_variant'], inplace=True)
    
    cli_df.to_excel(writer, sheet_name = 'unique_clinical' ,index = False, header=True)
    val_df.to_excel(writer, sheet_name = 'unique_validation' ,index = False, header=True)

    writer.save()
    return

if __name__ == '__main__':
    run()