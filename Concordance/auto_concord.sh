#!/bin/bash
#$ -j y
#$ -cwd
#$ -S /bin/bash
#$ -pe mpi 8
#$ -q test.q
#$ -M dlin@ambrygen.com
#$ -m a
#!/bin/bash
set -eux

WORK_DIR=$PWD
# cp original ava files to ./ori before start, and have a list of targeted runs in "runs" txt file
# Part1, this copies validatin ava files
copy_ava(){
        local run=$1
        
        # Regular handling, first have to cp the ori ava file with copy_ava.py
        mkdir ${run}
        cd ${run}
        # cp the ori ava that we collected with "copy_ava.py"
        cp -r ../ori .
        rm -f ./val/*NEG*

        # make a RD_acc,cli_acc key file "acc_key"
        ls ./val | awk -F"." '{print $1}' > acc_lst
        sed 's/RD_//' acc_lst | awk '{print substr($1,1,9)}' > acc_lst2
        # sed -e 's/^/RD_/' acc_lst > acc_lst2
        paste -d"," acc_lst acc_lst2 > acc_key


        # This needs ./ori, ./val, comma separated val_acc,cli_acc key file, and output folder
        # the output is concordant.csv, unique_clinical, unique_validation
        /usr/bin/python ${WORK_DIR}/concordance_check_artf_category_tc_filtered_RD_v2.py ./ori/ ./val/ acc_key ./

        # scrapes information from ava3
        /home/dlin/miniconda3/envs/ambry/bin/python3.7 ${WORK_DIR}/scrape_v2.py
        mv test.xlsx ${run}_concordance.xlsx
        cp *.xlsx $WORK_DIR
        cd $WORK_DIR
}
for run in `cat runs`; do copy_ava ${run} & done

# # # Part2: Getting all unannotated variants
# # concord_lst=$1
# # current_db=$2
# # # Get the variants that need to be annotated
# # # If it throw an error it might be because there is no variants that need to be annotated
# # python variant_dictionary.py concordant_xlsx_lst current_db_xlsx
# /home/dlin/miniconda3/envs/ambry/bin/python3.7 ${WORK_DIR}/variant_dictionary.py ${concord_lst} ${current_db}

# # # Part3: Parse concordance summary, this needs './*_concordance.xlsx' and 'var_db_v3.xlsx'
# /home/dlin/miniconda3/envs/ambry/bin/python3.7 ${WORK_DIR}/add_info2_get_summary.py
