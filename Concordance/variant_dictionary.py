from bs4 import BeautifulSoup
import requests
import re
import urllib.request
import pandas as pd
import sys, os, glob

"""Feed this script with a list of concordance results xlsx that bas 'unique_cli' and 'unique_val' sheets to be fully annotated, 
    the var_db xlsx that has variant 'info2' information,  this will parse and produce an excel 'Annota'ToBeAnno.xlsx' and 'svg' folder
    with nucleotide feature that you can review and add the info to the existing db """

concord_xlsxs = sys.argv[1]
# current_db = './var_db_v2.xlsx'
current_db = sys.argv[2]

session = requests.Session()
payload = {'email_address':'dlin@ambrygen.com', 
          'password':'A478478ambry8'}
s = session.post("http://ava3.ambrygen.com/authentication/login", data=payload)

classification_category = r'(New|new|NEW|Unclassified|unclassified|UNCLASSIFIED|Mutation|mutation|MUTATION|VLP|VUS|VLB|Silent_DNR|silent_DNR|SILENT_DNR|Poly|poly|POLY|Auto_filter|auto_filter|AUTO_FILTER|Category_6|category_6|CATEGORY_6)'

def get_record_v2(row):

    """ This funciton takes a row and returns classification and record info
            and save a seq.svg file"""

    gene = row['Gene'].strip()
    iso = row['Isoform'].strip()
    var = row['C_variant'].strip()
    print(gene, iso, var)
    
    var_changed = var.replace('+', '%2B')
    s = session.get(f'http://ava3.ambrygen.com/alterations/alterationshow?alteration={iso}+{var_changed}')
    soup = BeautifulSoup(s.text, 'html.parser')

    classification_div =  soup.findAll("div", {"class":"ui-widget-content vertical-middle"})[0]
    classification = re.findall( classification_category, str(classification_div))[0]
    print(classification)

    if classification == 'New' or classification == 'new' or classification == 'NEW':
        img_id = soup.findAll("img", {"id":"nucleotide_image"})[0]
        img = re.findall( r'<img id="nucleotide_image" src="(\/alterations.*)">', str(img_id))[0]
        img_url = "http://ava3.ambrygen.com/" + img
        img_name_var = var.replace('+', '').replace('>', '').replace('*', '')
        os.makedirs('./svg', exist_ok=True)
        urllib.request.urlretrieve(img_url, f"./svg/{gene}_{img_name_var}.svg")
        return f'{gene} {iso} {var} {classification}, complete'

    record_div = soup.findAll("div", {"class":"previous_records_section margin"})
    # print(record_div)
    record_inter = ', '.join([str(r) for r in record_div])
    record_long = re.findall( r'<span id="previous_records_count">((.*\n)+).*<\/p>', str(record_inter))

    if len(record_long) > 0:
        record = record_long[0][0].replace(' ', '').replace('\n', ' ').replace('</span>', ' ').strip()
        record = re.sub(r'Withinthose[\d\,]+record\(s\)\,', '', record)
        record_num = re.findall( r'([\d,\,]+\s+record\(s\))', record.split('.')[0])[0].replace(' record(s)', 'observations')
        record_info = record.split('.')[1]

        img_id = soup.findAll("img", {"id":"nucleotide_image"})[0]
        img = re.findall( r'<img id="nucleotide_image" src="(\/alterations.*)">', str(img_id))[0]
        img_url = "http://ava3.ambrygen.com/" + img
        img_name_var = var.replace('+', '').replace('>', '').replace('*', '')
        os.makedirs('./svg', exist_ok=True)
        urllib.request.urlretrieve(img_url, f"./svg/{gene}_{img_name_var}.svg")
        return f'{classification}, {record_num}, {record_info}'.replace('None, ', '')
    elif len(record_long) == 0:
        return f'{gene} {iso} {var} record not found, complete'

def get_var_df(lst_of_concord_results):
    """This compiles all the unique variants"""
    concord_lst = []
    with open(lst_of_concord_results) as f:
        for line in f:
            concord_lst.append(line.strip())
    df_lst = []
    for xlsx in concord_lst:
        print(f'doing {xlsx}')
        ucli_df = pd.read_excel(xlsx, sheet_name='unique_clinical')
        uval_df = pd.read_excel(xlsx, sheet_name='unique_validation')
        df_lst.append(ucli_df)
        df_lst.append(uval_df)
    var_df = pd.concat(df_lst, axis=0)
    return var_df

def run():
    # concord_xlsxs = glob.glob('concordance_*.xlsx')
    var_df = get_var_df(concord_xlsxs)
    # read the variants database df
    db_df = pd.read_excel(current_db )
    mdf = pd.merge(var_df, db_df, how='left', on=['Gene', 'Isoform', 'C_variant'])
    # find all the var that need to be annotated
    info2Na_df = mdf[mdf.info2.isna()]
    if len(info2Na_df) == 0:
        print('There is no variants need to be annotated')
        return
    gdf = info2Na_df.groupby(['Gene', 'Isoform', 'C_variant']).count()
    gdf = gdf[['info2']]
    gdf.reset_index(inplace=True)
    print(gdf)
    # gdf.to_excel('2beAnno.xlsx', index=False)
    # This not only gets info column but also the svg files
    gdf['info'] = gdf.apply(get_record_v2, axis = 1)
    print(gdf)
    gdf.to_excel('ToBeAnno.xlsx', sheet_name = 'ReviewNeeded' ,index = False)


if __name__=="__main__":
    run()