import os, sys, glob, shutil

"""This script reads in a runid,acc list and cp the ava.txt file over for concordance check"""

run_acc_lst = sys.argv[1] # comma separated list of runid,acc
working_dir = os.getcwd()


def read_lst(txt):
    lst = []
    with open(txt, 'r') as f:
        for line in f:
            runid = line.strip().split(',')[0]
            acc = line.strip().split(',')[1]
            lst.append( (runid, acc))
    return lst

def copy_ava(lst):
    for e in lst:
        runid = e[0]
        acc = e[1]
        ori = glob.glob( f'/NGSData/AVADeposit/77_CancerV5/{runid}/*{acc}*.ava.txt') # There are cases
        val = os.path.join(working_dir, f'{acc}.ava.txt')
        print(ori, val)
        shutil.copy2(ori, val)

def run():
    lst = read_lst(run_acc_lst)
    copy_ava(lst)

if __name__=="__main__":
    run()