#!/usr/bin/python
# Check line 90 for special acc name handling
# This script takes 3 arguments, use it as: /usr/bin/python script.py clinical_ava_folder val_ava_folder output_folder
# This script will look at the validation report folder and use the ava file inside to do the concordance check, lines in key: RD_acc,cli_acc
# /usr/bin/python concordance_check_artf_category_tc_filtered_RD_v2.py ./ori/ ./val/ key ./
# change the if statment section around "v5_ava" to handle the RD_ prefix of validation ava files (line 71)(Need to be reviewed)

import sys, os, re, glob, csv, MySQLdb

ngs_db = MySQLdb.connect(host='usav1svsqlp70',user='bio_user', passwd='bio@ambry20', db='ngs_panels')
cur = ngs_db.cursor()

def write_csv( path, list, accession, clinical_accession ):
    file_exists = os.path.isfile(path)

    with open(path, "a") as out:
        outWriter = csv.writer(out)

        if not file_exists:
            outWriter.writerow(("Validation Accession", "Accession", "Gene", "Isoform", "C_variant", "P_variant", "Zygosity", "Ref read count", "Alt read count", "Qscore",  "Het ratio"))

        for i in list:
            i = i.split(',')
            if re.match('(\S+)\s(.._\d*)\s(c\S+)\s(p\S+)\s(\S+)', i[0]):# concordant call with p.
                j = re.sub(r'\[|\]', "", i[0])
                j = re.split('\s', j)
                outWriter.writerow((accession, clinical_accession, j[0], j[1], j[2], j[3], j[4], i[1], i[2], i[3], i[4]))

            elif re.match('(\S+)\s(.._\d*)\s(c\S+)\s(\[\S+\])\s(\[\S+\])', i[0]):# concordant call without p.
                j = re.sub(r'\[|\]', "", i[0])
                j = re.split('\s', j)
                outWriter.writerow((accession, clinical_accession, j[0],j[1],j[2],"-",j[3]+" "+j[4], i[1], i[2], i[3], i[4]))

            elif re.match('(\S+)\s(.._\d*)\s(c\S+)\s(\[\S+\])', i[0]):# call without p.
                j = re.sub(r'\[|\]', "", i[0])
                j = re.split('\s', j)
                outWriter.writerow((accession, clinical_accession, j[0],j[1],j[2],"-",j[3], i[1], i[2], i[3], i[4]))

            elif re.match('(\S+)\s(.._\d*)\s(c\S+)\s(p\S+)\s(\[\S+\])\s(\[\S+\])', i[0]):
                j = re.sub(r'\[|\]', "", i[0])
                j = re.split('\s', j)
                outWriter.writerow((accession, clinical_accession, j[0],j[1],j[2],j[3],j[4]+"-"+j[5], i[1], i[2], i[3], i[4]))

    return;

def read_todic(txt):
    """this reads comma separated key value pair to a dic"""
    dic = {}
    with open(txt, 'r') as f:
        for line in f:
            # print(line.split(','))
            RD_acc = line.strip().split(',')[0]
            cli_acc = line.strip().split(',')[1]
            dic[RD_acc] = cli_acc

    return dic

def show_dic(dic):
    for k,v in dic.items():
        print(k,v)

def read_lst(txt):
    lst = []
    with open(txt) as f:
        for line in f:
            lst.append(line.strip())
    return lst


v4_ava_dir = sys.argv[1] #folder that contains clinical ava files
v5_ava_dir = sys.argv[2] #folder that contains validation ava files
dic_txt = sys.argv[3] # RD_acc,Cli_acc key txt file
out_path = sys.argv[4] # output folder

Rd2Cli_dic = read_todic(dic_txt)


concord = "concordant.csv"
unique_v4_out = "unique_clinical.csv"
unique_v5_out = "unique_validation.csv"
clin = []
ava_v4_lines = []
ava_v5_lines = []
f_v4 = []
f_v5 = []
concordant_het = set()
unique_v4_het = []
unique_v5_het = []

v4_ava = [f for f in glob.glob(v4_ava_dir + "*.ava.txt")]
print len(v4_ava)

v5_ava = [g for g in glob.glob(v5_ava_dir + "*.ava.txt")] # validation ava files
#print v5_ava[:10]
print len(v5_ava)

for g in v5_ava:# change the if statment section to handle the RD_ prefix of validation ava files
  # Handle the odd names: 20_B12345_A
  try:
    acc = g.split('/')[-1].split('.')[0]
    clin_acc = Rd2Cli_dic[acc]
  except:
    try:
      print 'trying again with suffix removed'
      acc = g.split('/')[-1].split('.')[0][:9]
      clin_acc = Rd2Cli_dic[acc]
    except Exception as e:
        print e
        raise

  for f in v4_ava:
    #val_acc = f.split('/')[-1].split('.')[0]
    if clin_acc in f:
    #if acc in f:
      val_acc = f.split('/')[-1].split('.')[0]
      print f
      clin.append(clin_acc)

      with open(f) as v4_in:
        v4_in = set(v4_in.read().splitlines())
      with open(g) as v5_in:
        v5_in = set(v5_in.read().splitlines())

      for line in v4_in:
        if not line.startswith('#'):
          ava_v4_lines.append(line)
          if not re.findall(r'(noloco)|(DELDUP)', line):
            line = line.rstrip()
            line = re.sub(r'\[\d+\S+\]', '', line)
            f_v4.append(line)
        
      for line2 in v5_in:
        if not line2.startswith('#'):
          ava_v5_lines.append(line2)
          if not re.findall(r'(noloco)|(DELDUP)', line2):
            line2 = line2.rstrip()
            line2 = re.sub(r'\[\d+\S+\]', '', line2)
            f_v5.append(line2)

      unique_v4 = list(set(f_v4) - set(f_v5))
      unique_v5 = list(set(f_v5) - set(f_v4))
      concordant = list(set(f_v4).intersection(f_v5))
      for t in concordant:
        concordant_het.add(','.join([t, "-", "-", "-", "-"]))

      ##Check cdot only, no genotype
      ##Include cdot in concordant list if same cdot was detected but with different genotype
      #for k in unique_v4:
        #if k != '':
          #gene = k.split(' ')[0]
          #isof = k.split(' ')[1]
          #cdot = k.split(' ')[2]

          #for m in unique_v5:
            #if m != '':
              #m_gene = m.split(' ')[0]
              #m_isof = m.split(' ')[1]
              #m_cdot = m.split(' ')[2]

              #if gene == m_gene and isof == m_isof and cdot == m_cdot:
                #concordant_het.add(','.join([m, "-", "-", "-", "-"]))
                #unique_v4.remove(k)
                #unique_v5.remove(m)

      print len(concordant_het)
      write_csv( out_path+"/"+concord, concordant_het, acc, val_acc )

      print len(unique_v4)
      print len(unique_v5)
      print unique_v4[:3]
      # check all the calls in unique v4
      for k in unique_v4:
        k = k.rstrip()
        for a in range(0, len(ava_v4_lines)):
          if k in ava_v4_lines[a]:
            if k != '':
              gene = k.split(' ')[0]
              isof = k.split(' ')[1]
              cdot = k.split(' ')[2]
              cur = ngs_db.cursor()
              # Check if it is artifact
              artf_query = "SELECT * FROM `artifact_var_list` WHERE `symbol` = '{0}' AND `nucleotide_id` = '{1}' AND `c_variant` REGEXP '{2}'".format(gene, isof, cdot)
              cur.execute(artf_query)
              artf_count = cur.rowcount
              cur.close()
              # if it is not artifact, check the variant category
              if artf_count == 0:
                cur = ngs_db.cursor()
                category_query = "SELECT `category_updated` FROM `variant_records` WHERE `symbol` = '{0}' AND `nucleotide_id` = '{1}' AND LOWER(`c_variant`) = LOWER('{2}')".format(gene, isof, cdot)
                cur.execute(category_query)
                categ_res = cur.fetchall()
                categ_res_count = cur.rowcount
                cur.close()
                for cat in categ_res:
                  cat = int(cat[0])
                # if it the the first 4 category: Unclassified, Mutation, VLP, and VUS. Calculate the het ratio
                if (categ_res_count == 0 or cat < 5):
                    reads = re.findall('\[(\d+)\.(\d+)\.\d+\:q(\d+)\]', ava_v4_lines[a])
                    for r in reads:
                      ref = float(r[0])
                      alt = float(r[1])
                      qscore = int(r[2])
                      #check "clear-by-stat"(don't think it checked clear by stat here) indels with MUT/VLP protected
                      if qscore < 200 and re.search('(ins|del|dup)', cdot):
                        cur = ngs_db.cursor()
                        var_categ_query = "SELECT `category_updated` FROM `variant_records` WHERE `symbol` = '{0}' AND `nucleotide_id` = '{1}' AND `c_variant` REGEXP '{2}'".format(gene, isof, cdot)
                        cur.execute(var_categ_query)
                        var_categ = cur.fetchall()
                        cur.close()
                        # if it is mutation or VLP, append to the list
                        if var_categ in (2, 3):
                          if ref == 0:
                            unique_v4_het.append(','.join([k, str(ref), str(alt), str(qscore), "-"]))
                          else:
                            het_ratio = float(alt / (ref + alt))
                            unique_v4_het.append(','.join([k, str(ref), str(alt), str(qscore), str(round(het_ratio,2))]))

                      #check for reporting range & exceptions
                      else:
                        cur = ngs_db.cursor()
                        exception_query = "SELECT * FROM `reporting_range_var_exception` WHERE `symbol` = '{0}' AND `nucleotide_id` ='{1}' AND `c_variant` REGEXP '{2}'".format(gene, isof, cdot)
                        cur.execute(exception_query)
                        exception_count = cur.rowcount
                        cur.close()
                        # if it is not in the reporting range exception list
                        if exception_count == 0:
                          #regexp = re.search('c\.(\d+)([\+|\-]\d+)?(\_)?(\d+)?([\+|\-]\d+)?', cdot)
                          regexp = re.search('c\.(\d+)?([\+|\-])?(\d+)?(\_)?(\d+)?([\+|\-])?(\d+)?', cdot)
                          try:
                            intron_pos1 = int(regexp.group(3))
                            intron_pos2 = int(regexp.group(7))
                          except:
                            try:
                              intron_pos1 = int(regexp.group(3))
                              intron_pos2 = '-'
                            except:
                              intron_pos1 = '-'
                              intron_pos2 = '-'
                      
                          if ((intron_pos1 == '-' and intron_pos2 == '-') or (intron_pos1 <= 5 and intron_pos2 == '-') or (intron_pos1 <= 5 or intron_pos2 <= 5)):
                            if ref == 0:
                              unique_v4_het.append(','.join([k, str(ref), str(alt), str(qscore), "-"]))
                            else:
                              het_ratio = float(alt / (ref + alt))
                              unique_v4_het.append(','.join([k, str(ref), str(alt), str(qscore), str(round(het_ratio,2))]))
                        # If it is in the exception list
                        else:
                          if ref == 0:
                            unique_v4_het.append(','.join([k, str(ref), str(alt), str(qscore), "-"]))
                          else:
                            het_ratio = float(alt / (ref + alt))
                            unique_v4_het.append(','.join([k, str(ref), str(alt), str(qscore), str(round(het_ratio,2))]))

      write_csv( out_path+"/"+unique_v4_out, unique_v4_het, val_acc, acc )

      #unique_v5 = list(set(f_v5) - set(f_v4))
      for m in unique_v5:
        m = m.rstrip()
        print "m: ", m
        #print ava_v5_lines[:3]
        for b in range(0, len(ava_v5_lines)):
          if m in ava_v5_lines[b]:
            cur = ngs_db.cursor()
            print "m in ava_v5: ", m
            m_gene = m.split(' ')[0]
            m_isof = m.split(' ')[1]
            m_cdot = m.split(' ')[2]
            m_artf_query = "SELECT * FROM `artifact_var_list` WHERE `symbol` = '{0}' AND `nucleotide_id` = '{1}' AND `c_variant` REGEXP '{2}'".format(m_gene, m_isof, m_cdot)
            cur.execute(m_artf_query)
            m_artf_count = cur.rowcount
            cur.close()

            #if m_artf_count > 0:
            #  print "Artifact: ", m

            if m_artf_count == 0:
              print "not artifact"
              cur = ngs_db.cursor()
              m_category_query = "SELECT `category_updated` FROM `variant_records` WHERE `symbol` = '{0}' AND `nucleotide_id` = '{1}' AND LOWER(`c_variant`) = LOWER('{2}')".format(m_gene, m_isof, m_cdot)
              #print m_gene, m_isof, m_cdot
              cur.execute(m_category_query)
              m_categ_res = cur.fetchall()
              m_categ_res_count = cur.rowcount
              cur.close()
              #print m_categ_res_count
              for m_cat in m_categ_res:
                  m_cat = int(m_cat[0])
                  print "classification: ", str(m_cat)

              if (m_categ_res_count == 0 or int(m_cat < 5)) :
                  print "0 count in variant records or category < 5 "
                  #print m_categ_res[0]
                  print m_gene, m_cdot 
                  reads = re.findall('\[(\d+)\.(\d+)\.\d+\:q(\d+)\]', ava_v5_lines[b])
                  for r in reads:
                    ref = float(r[0])
                    alt = float(r[1])
                    qscore = int(r[2])

                    #check clear-by-stat indels with MUT/VLP protected
                    if qscore < 200 and re.search('(ins|del|dup)', m_cdot):
                      cur = ngs_db.cursor()
                      m_var_categ_query = "SELECT `category_updated` FROM `variant_records` WHERE `symbol` = '{0}' AND `nucleotide_id` = '{1}' AND `c_variant` REGEXP '{2}'".format(m_gene, m_isof, m_cdot)
                      cur.execute(m_var_categ_query)
                      m_var_categ = cur.fetchall()
                      cur.close()
                      if m_var_categ in (2, 3):
                        print "protect mut and vlp"
                        if ref == 0:
                          unique_v5_het.append(','.join([m, str(ref), str(alt), str(qscore), "-"]))
                        else:
                          het_ratio = float(alt / (ref + alt))
                          unique_v5_het.append(','.join([m, str(ref), str(alt), str(qscore), str(round(het_ratio,2))]))

                      #else:
                      #  print "clear-by-stat"

                    #check for reporting range & exceptions
                    else:
                      cur = ngs_db.cursor()
                      m_exception_query = "SELECT * FROM `reporting_range_var_exception` WHERE `symbol` = '{0}' AND `nucleotide_id` ='{1}' AND `c_variant` REGEXP '{2}'".format(m_gene, m_isof, m_cdot)
                      cur.execute(m_exception_query)
                      m_exception_count = cur.rowcount
                      cur.close()

                      if m_exception_count == 0:
                        print "not exception"
                        #regexp = re.search('c\.(\d+)([\+|\-]\d+)?(\_)?(\d+)?([\+|\-]\d+)?', m_cdot)
                        regexp = re.search('c\.(\d+)?([\+|\-])?(\d+)?(\_)?(\d+)?([\+|\-])?(\d+)?', m_cdot)
                        try:
                          intron_pos1 = int(regexp.group(3))
                          intron_pos2 = int(regexp.group(7))
                        except:
                          try:
                            intron_pos1 = int(regexp.group(3))
                            intron_pos2 = '-'
                          except:
                            intron_pos1 = '-'
                            intron_pos2 = '-'

                        print m_cdot, intron_pos1, intron_pos2
                        if ((intron_pos1 == '-' and intron_pos2 == '-') or (intron_pos1 <= 5 and intron_pos2 == '-') or (intron_pos1 <= 5 or intron_pos2 <= 5)):
                          print "either one or both intron_pos are within reporting range"
                          if ref == 0:
                            unique_v5_het.append(','.join([m, str(ref), str(alt), str(qscore), "-"]))
                          else:
                            het_ratio = float(alt / (ref + alt))
                            unique_v5_het.append(','.join([m, str(ref), str(alt), str(qscore), str(round(het_ratio,2))]))
                      
                      else:
                        print "all else"
                        if ref == 0:
                          unique_v5_het.append(','.join([m, str(ref), str(alt), str(qscore), "-"]))
                        else:
                          het_ratio = float(alt / (ref + alt))
                          unique_v5_het.append(','.join([m, str(ref), str(alt), str(qscore), str(round(het_ratio,2))]))

      write_csv( out_path+"/"+unique_v5_out, unique_v5_het, acc, val_acc )

      f_v4 = []
      f_v5 = []
      ava_v4_lines = []
      ava_v5_lines = []
      concordant_het = set()
      unique_v4_het = []
      unique_v5_het = []

  print len(clin)
