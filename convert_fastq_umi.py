#!/home/zlei/anaconda3/envs/py3/bin/python3
import sys
import os
import gzip
import datetime
import logging
import argparse
python_lib = os.path.join(os.getenv("HOME"), "python_lib")
if not python_lib in sys.path: sys.path.append(python_lib)
from fs3 import FS

log_level = logging.DEBUG
logging.basicConfig(filename=None, format='%(asctime)s [%(levelname)s]: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=log_level)

def process(lines=None):
    ks = ['name', 'sequence', 'optional', 'quality']
    return {k: v for k, v in zip(ks, lines)}


def main():
	fs = FS()
	args = get_arguments()
	#gr = pr.PyRanges(pd.read_csv(args.bed_file, usecols=[0,1,2], names=["Chromosome", "Start", "End"], sep='\t'))
	#print("PyRanges is initialized!")
	#sample = os.path.basename(args.per_base_bed_gz).replace(".per-base.bed.gz", "")
	inr1_file = args.input_fastq_R1_file
	input_folder = os.path.dirname(inr1_file)
	r1_basename = os.path.basename(inr1_file)
	r2_basename = r1_basename.replace("_R1", "_R2")

	inr2_file = os.path.join(input_folder, r2_basename)
	outr1_file = os.path.join(args.output_dir, r1_basename)
	outr2_file = os.path.join(args.output_dir, r2_basename)

	inr1 = gzip.open(inr1_file, 'rt')
	inr2 = gzip.open(inr2_file, 'rt')
	outr1 = gzip.open(outr1_file, 'wt')
	outr2 = gzip.open(outr2_file, 'wt')

	print("R2 in_file:" + inr2_file)
	print("R2 out_file:" + outr2_file)

	inlines1 = inr1.readlines()
	inlines2 = inr2.readlines()

	if len(inlines1) != len(inlines2):
		print('R1 and R2 have different number of reads, something is wrong!')
	else:
		read_num = int(len(inlines1)/4)
		for i in range(read_num):
			r1_id = inlines1[4*i]
			r1_seq = inlines1[4*i+1]
			r1_strand = inlines1[4*i+2]
			r1_q = inlines1[4*i+3]
			
			r2_id = inlines2[4*i]
			r2_seq = inlines2[4*i+1]
			r2_strand = inlines2[4*i+2]
			r2_q = inlines2[4*i+3]
			
			newr1_seq = r1_seq[:3]+r2_seq[0:3]+r1_seq[3:]
			newr1_q = r1_q[:3]+r2_q[0:3]+r1_q[3:]
			newr2_seq = r2_seq[5:]
			newr2_q = r2_q[5:]
			
			outr1.write(r1_id)
			outr1.write(newr1_seq)
			outr1.write(r1_strand)
			outr1.write(newr1_q)
			
			outr2.write(r2_id)
			outr2.write(newr2_seq)
			outr2.write(r2_strand)
			outr2.write(newr2_q)            
			
		inr1.close()
		inr2.close()
		outr1.close()
		outr2.close()
						


def get_arguments():
	main_description = '''\


Author: Jingyi Lu and Zhengdeng Lei 

This script will convert fastq files from duplex mode to simplex mode.
original script is written by Jingyi Lu:
/mnt/jlu/projects/convert_fastq/Convert_Fastq_UMI.py

	'''
	epi_log='''
This program requires the python packages:
argparse
logging
	'''	
	help_help = '''\
	show this help message and exit\
	'''
	version_help = '''\
	show the version of this program\
	'''

	input_fastq_R1_file_help = '''\
	input fastq file
	'''

	output_dir_help = '''\
	output directory to store the simplex mode fastq files
	default ./simplex/
	'''

	arg_parser = argparse.ArgumentParser(description=main_description, epilog=epi_log, formatter_class=argparse.RawTextHelpFormatter, add_help=False)
	arg_parser.register('type','bool', lambda s: str(s).lower() in ['true', '1', 't', 'y', 'yes']) # add type keyword to registries
	###############################
	#    1. required arguments    #
	###############################
	required_group = arg_parser.add_argument_group("required arguments")

	###############################
	#    2. optional arguments    #
	###############################
	optional_group = arg_parser.add_argument_group("optional arguments")
	optional_group.add_argument("-o", dest="output_dir", default="simplex", help=output_dir_help)	
	
	###############################
	#    3. positional arguments  #
	###############################
	arg_parser.add_argument('input_fastq_R1_file', default=None, help=input_fastq_R1_file_help)


	optional_group.add_argument("-h", "--help", action="help", help=help_help)
	optional_group.add_argument("-v", "--version", action="version", version="%(prog)s: version 1.0", help=version_help)
	args = arg_parser.parse_args()



	abs_paths = ["input_fastq_R1_file", "output_dir"]
	create_dirs = []
	must_exist_paths = ["input_fastq_R1_file", "output_dir"]
	check_arguments(args, arg_parser, abs_paths, create_dirs, must_exist_paths)
	return args

def check_arguments(args, arg_parser, abs_paths, create_dirs, must_exist_paths):
	for abs_path in abs_paths:
		obj = eval("args." + abs_path)
		if isinstance(obj, list):
			exec("args.{abs_path} = [os.path.abspath(x) for x in args.{abs_path}]".format(abs_path=abs_path))  in globals(), locals()
		else:
			exec("args.{abs_path} = os.path.abspath(args.{abs_path})".format(abs_path=abs_path))  in globals(), locals()

	for create_dir in create_dirs:
		dir_path = eval("args." + create_dir)
		if not os.path.exists(dir_path):
			os.makedirs(dir_path)
			logging.debug("%s is created!\n" % dir_path)

	for must_exist_path in must_exist_paths:
		obj = eval("args." + must_exist_path)
		if isinstance(obj, list):
			for path in obj:
				if not os.path.exists(path):
					arg_parser.print_help()
					logging.error("No file exist as \"%s\" \n" % path )
					sys.exit()					
		else:
			path = obj
			if not os.path.exists(path):
				arg_parser.print_help()
				logging.error("\"%s\" does not exist!!!\n" % path )
				sys.exit()

if __name__ == '__main__':
	main()
