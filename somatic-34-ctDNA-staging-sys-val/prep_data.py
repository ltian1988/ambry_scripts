from os.path import join
import os
import pandas as pd

SOURCE_FOLDER = '/mnt/zlei/projects/ctDNA/SystemsTestingRun/Unaligned_RAW/278_Liquid'
SAMPLESHEET = '/mnt/zlei/projects/ctDNA/SystemsTestingRun/990621E2E_NovaSeq_tagsetC_baitcap.fixed.csv'
TARGET_FOLDER = '/NGS/Novaseq/220627_A01310_0372_AHHT53DRX2'
UNALIGNED_FOLDER = join(TARGET_FOLDER, 'Unaligned_SGE')
DEMUX_FOLDER = join(UNALIGNED_FOLDER, 'Project_278_Liquid')


if __name__ == '__main__':
    samples = pd.read_csv(SAMPLESHEET, sep=',', header=0, skiprows=14)
    for i in samples.index:
        sample = samples.loc[i, 'Sample_ID']
        sample_dir = join(DEMUX_FOLDER, 'Sample_'+sample)
        os.mkdir(sample_dir)
        os.system(f'cp {SOURCE_FOLDER}/{sample}_* {sample_dir}/')