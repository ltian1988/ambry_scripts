#!/bin/bash
#$ -j y
#$ -cwd
#$ -S /bin/bash
#$ -pe mpi 1
#$ -q low.q
#$ -M ltian@ambrygen.com
#$ -m a

# set -eux
WORK_DIR=$PWD

rsync -agvP --exclude={'Aligned_*','.demultiplex_*','nohup_*','status.quality_pass','qual_metrics_warnings.txt','Thumbnail_Images','jobs'} \
            /mnt/zlei/projects/ctDNA/SystemsTestingRun/Unaligned_RAW/Reports/ /NGS/Novaseq/220627_A01310_0372_AHHT53DRX2/Unaligned_SGE/Reports
