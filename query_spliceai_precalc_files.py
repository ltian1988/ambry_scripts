from venv import main
import pysam
import sys
import subprocess


snv_score_file = '/mnt/spliceai/scores/spliceai_scores.masked.snv.hg19.vcf.gz'
indel_score_file = '/mnt/spliceai/scores/spliceai_scores.masked.indel.hg19.vcf.gz'

def query_and_write_each_record(record, out_handler):
    chrom = record.chrom.strip('chr')
    pos = record.pos
    ref = record.ref
    alt = record.alts[0]
    if len(ref) == len(alt) == 1:
        score_file = snv_score_file
    else:
        score_file = indel_score_file
    cmd = f"""/home/ltian/anaconda3/bin/tabix -h {score_file} {chrom}:{pos}-{pos} | /home/ltian/anaconda3/bin/bcftools filter -i "POS={pos} && REF='{ref}' && ALT='{alt}'" | /home/ltian/anaconda3/bin/bcftools query -f '%INFO/SpliceAI'"""
    p = subprocess.run(
        cmd,
        shell=True,
        capture_output=True,
        encoding="ascii",
        check=True
    )
    res = p.stdout
    record.info["SpliceAI"] = res
    out_handler.write(record)

def query_each_record(record):
    chrom = record.chrom.strip('chr')
    pos = record.pos
    ref = record.ref
    alt = record.alts[0]
    if len(ref) == len(alt) == 1:
        score_file = snv_score_file
    else:
        score_file = indel_score_file
    cmd = f"""/home/ltian/anaconda3/bin/tabix -h {score_file} {chrom}:{pos}-{pos} | /home/ltian/anaconda3/bin/bcftools filter -i "POS={pos} && REF='{ref}' && ALT='{alt}'" | /home/ltian/anaconda3/bin/bcftools query -f '%INFO/SpliceAI'"""
    p = subprocess.run(
        cmd,
        shell=True,
        capture_output=True,
        encoding="ascii",
        check=True
    )
    res = p.stdout
    record.info["SpliceAI"] = res


def query_vcf(in_vcf, out_vcf, cores):
    vcf = pysam.VariantFile(in_vcf)
    header = vcf.header
    header.add_line('##INFO=<ID=SpliceAI,Number=.,Type=String,Description="SpliceAIv1.3.1 variant '
                    'annotation. These include delta scores (DS) and delta positions (DP) for '
                    'acceptor gain (AG), acceptor loss (AL), donor gain (DG), and donor loss (DL). '
                    'Format: ALLELE|SYMBOL|DS_AG|DS_AL|DS_DG|DS_DL|DP_AG|DP_AL|DP_DG|DP_DL">')
    out_handler = pysam.VariantFile(out_vcf, mode = 'w', header=header)
    # for record in vcf:
    #     query_and_write_each_record(record, out_handler)
    pool = mp.Pool(cores)
    # partial_query = partial(query_each_record, out_handler=out_handler)
    pool.map(query_each_record, vcf)
    for record in vcf:
        out_handler(record)
    vcf.close()
    out_handler.close()

if __name__ == '__main__':
    query_vcf(sys.argv[1], sys.argv[2], int(sys.argv[3]))
