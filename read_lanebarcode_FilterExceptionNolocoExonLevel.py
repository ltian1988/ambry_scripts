from bs4 import BeautifulSoup
import pandas as pd
import sys, glob, os
import sqlalchemy as db

"""This will work when there are multiple panels like 72, 76, 77 all in the same run
    python read_lanebarcodeV2.py $runid
    """

runid = sys.argv[1]
acc_key = sys.argv[2]

html = glob.glob(f'/NGS/*seq/{runid}/Unaligned_SGE/Reports/html/*/all/all/all/laneBarcode.html')[0]

def read_todic(txt):
    """this reads comma separated key value pair to a dic"""
    dic = {}
    with open(txt, 'r') as f:
        for line in f:
            RD_acc = line.strip().split(',')[0]#validation samples
            cli_acc = line.strip().split(',')[1]
            dic[RD_acc] = cli_acc
    return dic

def show_dic(dic):
    for k,v in dic.items():
        print(k,v)

def status_no_noloco(row):
    """ this function looks at the cells in a row so need to use axis = 1 during apply"""
    if row['% Perfectbarcode'] > 85 and row['% >= Q30bases'] > 75 and row['Mean QualityScore'] > 30:
        return 'PASS'
    return 'FAIL'

def status_w_noloco(row):
    """ this function looks at the cells in a row so need to use axis = 1 during apply"""
    if row['% Perfectbarcode'] > 85 and row['% >= Q30bases'] > 75 and row['Mean QualityScore'] > 30 and row['Noloco'] <= 10:
        return 'PASS'
    return 'FAIL'

def get_testcode_from_acc(acc):
    # connect to the p70 server
    engine = db.create_engine('mysql+pymysql://reporter:123@usav1svsqlp70/ngs_panels')
    connection = engine.connect()
    q = f"SELECT panel_code FROM patient_status_panelcode WHERE accession_id = '{acc}';"
    df = pd.read_sql_query(q, connection)
    connection.close()
    return df

def get_genes_from_testcode(tc, acc):
    engine = db.create_engine('mysql+pymysql://pipeline:xfZsZNTQLwwFcuqX@usav1svhapp10/portal')
    connection = engine.connect()
    q = f"CALL CalculateOrderedGenes('{tc}', '{acc}');"
    df = pd.read_sql_query(q, connection)
    connection.close()
    return df

def get_testcode_gene_for_acc(RdAcc, acc_key):
    Rd2Cli_dic = read_todic(acc_key)
    acc = Rd2Cli_dic[RdAcc]

    acc = acc.replace('_', '-')
    panelcode_df = get_testcode_from_acc(acc)
    tc = panelcode_df['panel_code'].to_list()
    genes_df_lst = []
    for t in tc:
        df = get_genes_from_testcode(t,acc )
        genes_df_lst.append(df)
    cdf = pd.concat(genes_df_lst, axis=0)

    # df = get_genes_from_testcode(tc[1], acc)
    # cdf = df

    seq_gene_df = cdf[cdf['gene_methodology_type'] =='seq']
    dd_gene_df = cdf[cdf['gene_methodology_type'] =='dd']
    return seq_gene_df['gene_symbol'].to_list(), dd_gene_df['gene_symbol'].to_list()

def get_noloco_filter_seq_genes(runid, acc, seq_genes):
    sample_noloco = glob.glob(f'/NGS/N*seq/{runid}/Aligned_Panel_77_SGE/Project_77_CancerV5/Report_Files/{acc}.noloco.csv')[0]
    print(sample_noloco)
    df = pd.read_csv(sample_noloco, sep=',', header=0)
    df['runid'] = runid
    df['sample'] = acc
    # filter by panel code
    df = df[ df['Gene'].isin(seq_genes)]
    # get exon level noloco
    return df

def run():
    df_lst = pd.read_html(html)

    df = df_lst[2]
    df.columns = df.columns.droplevel(0)
    df.drop(['#', 'Project', 'Barcode sequence', r'% of thelane', r'% One mismatchbarcode', r'% PFClusters'], inplace=True, axis=1)
    # df = df.loc[:,~df.columns.duplicated()] # keep all the rows and remove duplicate columns, duplicated() returns boolean array, it is flipped by "~"
    df = df.groupby('Sample').sum()
    # df.drop(['unknown'], inplace=True, axis=0)
    df = df/4 # to get the avg num/ lane, there are 4 lanes in nextseq
    df['Clusters'] = df['Clusters']*4 # there is no need to get avg cluster
    df['Yield (Mbases)'] = df['Yield (Mbases)']*4
    df = df.applymap(lambda x: float(f'{x:.1f}')) # formats the numbers to 1 digit after desimal point
    df = df.reset_index()
    df = df[~(df.Sample == 'unknown')]
    # print(df.head())


    # filter the no coverage exceptions
    exception_lst = [
        'NM_000251_2_47706634', 'NM_000051_11_108102850', 'NM_000051_11_108167072', 'NM_000059_13_32941155', 'NM_000267_17_29468403', 'NM_004360_16_68856919', 'NM_000051_11_108106895', 'NM_000267_17_29468403'
    ]
    noloco_glob = glob.glob(f'/NGS/*seq/{runid}//Aligned_Panel_7?_SGE/Project_*/Report_Files/*.noloco.csv')
    noloco_df_lst = []
    for n in noloco_glob:
        sample_name = os.path.basename(n).split('.')[0]
        
        # with testcode filter
        seq_genes, dd_genes = get_testcode_gene_for_acc(sample_name, acc_key)
        ndf = get_noloco_filter_seq_genes(runid,sample_name, seq_genes)# filter noloco.csv with testcode

        # without testcode filtering 
        # ndf = pd.read_csv(n, sep=',', header=0)

        if len(ndf) > 0:
            
            ndf['runid'] = runid
            ndf['Sample'] = sample_name

            ndf['info'] =ndf.apply(lambda x: x['Isoform'] + "_" + str(x['Chr']) + "_" + str(x['Start']), axis=1  )
            print(ndf)
            ndf = ndf[ ~ndf['info'].isin(exception_lst)]
            print(ndf)
            noloco_df_lst.append(ndf)# nothing left

        
    cdf = pd.concat(noloco_df_lst, axis=0)
    # print(cdf)

    gcdf = cdf.groupby(['Sample', 'Gene', 'Isoform', 'Exon']).size().to_frame(name='NolocoAtCsv')
    gcdf.reset_index(inplace=True)
    ggcdf = gcdf.groupby('Sample').size().to_frame(name='Noloco')
    # ggcdf.to_excel('ggcdf.xlsx', index=True)
    ggcdf.reset_index(inplace=True)
    mdf = pd.merge(df, ggcdf, how='left', on='Sample')
    print(mdf)

    mdf['Status'] = mdf.apply(status_w_noloco, axis=1)
    writer = pd.ExcelWriter(f'{runid}_seqQC_exception_filtered_no_testcode_exon_level.xlsx', engine='xlsxwriter')
    mdf.to_excel(writer, sheet_name = f'{runid}' ,index = False)
    writer.save()

if __name__=="__main__":
    run()