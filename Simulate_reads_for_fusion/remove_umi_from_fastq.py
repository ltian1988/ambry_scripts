#!/home/zlei/anaconda3/envs/py3/bin/python3

import sys
import os
import logging
import argparse
import FASTQ1 as fastq1

__version__ = "1.0"
log_level = logging.DEBUG
logging.basicConfig(filename=None, format='%(asctime)s [%(levelname)s]: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=log_level)

def main():
	'''
	main function: 

	'''	

	args = get_arguments()
	#print(args.output_dir)
	#print(os.path.dirname(args.fastq_file))
	if args.suffix == "":
		if args.output_dir == os.path.dirname(args.fastq_file):
			logging.error("The output dir can't be the same as the dir of input fastq file!\nExit now!\n")
			sys.exit()

	converter = fastq1.FASTQ1(args.fastq_file, args.suffix, args.UMI_len)
	converter.convert()
	converter.write_out(args.output_dir)

	logging.info(os.path.basename(__file__) + " completed!")


def get_arguments():
	main_description = '''\
This script removes UMI from fastq file.
NOTE: BEFORE running this script, the output directory must be created if it does not exist.
This script will read the fastq file into memory.
Please make sure it has enough memory. Use q64 NOT q if it is submitted to HPC.
Author: Zhengdeng Lei (zlei@ambrygen.com)

Example:
%(prog)s *.vcf
	'''

	epi_log='''
This program requires the packages:
logging
argparse
	'''	

	help_help = '''\
	show this help message and exit\
	'''

	version_help = '''\
	show the version of this program\
	'''

	UMI_len_help = '''\
	UMI length, e.g. 5 for UMI structure 3M2S146T
	'''
	fastq_file_help = '''\
	One fastq file, e.g. Sample1_R1.fastq.gz
	'''

	suffix_help = '''\
	suffix of output, e.g. "withUMI": Sample1_R1.fastq.gz => Sample1_R1.withUMI.fastq.gz
	default is "", i.e. the same filename as input, in this case, you must output the file in a new folder.
	'''

	output_dir_help = '''\
	output directory
	default is current working diretory.
	'''

	arg_parser = argparse.ArgumentParser(description=main_description, epilog=epi_log, formatter_class=argparse.RawTextHelpFormatter, add_help=False)
	arg_parser.register('type','bool', lambda s: str(s).lower() in ['true', '1', 't', 'y', 'yes']) # add type keyword to registries

	###############################
	#    1. required arguments    #
	###############################
	required_group = arg_parser.add_argument_group("required arguments")
	#required_group.add_argument("-b", dest="bam_file",  action="store", required=True, help=bam_file_help)
	#required_group.add_argument("-vcf", dest="vcf_file",  action="store", required=True, help=vcf_file_help)
	#required_group.add_argument("-f", dest="info_field_suffix",  action="store", required=True, help=info_field_suffix_help)
	required_group.add_argument("-l", type=int, dest="UMI_len", required=True, help=UMI_len_help)

	###############################
	#    2. optional arguments    #
	###############################
	optional_group = arg_parser.add_argument_group("optional arguments")
	optional_group.add_argument("-s", dest="suffix",  action="store", default="", help=suffix_help)
	optional_group.add_argument("-o", dest="output_dir",  action="store", default=os.getcwd(), help=output_dir_help)

	###############################
	#    3. positional arguments  #
	###############################
	arg_parser.add_argument('fastq_file', metavar='FASTQ_FILE', help=fastq_file_help)


	optional_group.add_argument("-h", "--help", action="help", help=help_help)
	optional_group.add_argument("-v", "--version", action="version", version="%(prog)s: version " + __version__, help=version_help)
	args = arg_parser.parse_args()

	abs_paths = ["fastq_file", "output_dir"]
	create_dirs = []
	must_exist_paths = ["fastq_file", "output_dir"]
	check_arguments(args, arg_parser, abs_paths, create_dirs, must_exist_paths)
	return args

def check_arguments(args, arg_parser, abs_paths, create_dirs, must_exist_paths):
	'''
	check_arguments:
	(1) change the paths into absolute paths
	(2) create diretories if needed
	(3) check if the paths exists in the file system
	'''
	for abs_path in abs_paths:
		obj = eval("args." + abs_path)
		if isinstance(obj, list):
			# list of files
			exec("args.{abs_path} = [os.path.abspath(x) for x in args.{abs_path}]".format(abs_path=abs_path))  in globals(), locals()
		else:
			# single file
			exec("args.{abs_path} = os.path.abspath(args.{abs_path})".format(abs_path=abs_path))  in globals(), locals()

	for create_dir in create_dirs:
		dir_path = eval("args." + create_dir)
		if not os.path.exists(dir_path):
			os.makedirs(dir_path)
			logging.debug("%s is created!\n" % dir_path)

	for must_exist_path in must_exist_paths:
		obj = eval("args." + must_exist_path)
		if isinstance(obj, list):
			# list of files
			for path in obj:
				if not os.path.exists(path):
					arg_parser.print_help()
					logging.error("No file exist as \"%s\" \n" % path )
					sys.exit()					
		else:
			# single file
			path = obj
			if not os.path.exists(path):
				arg_parser.print_help()
				logging.error("\"%s\" does not exist!!!\n" % path )
				sys.exit()

if __name__ == '__main__':
	main()
