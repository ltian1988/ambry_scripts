import sys
from Bio.Seq import Seq
from Bio import SeqIO
import gzip
from Bio.SeqIO.QualityIO import FastqGeneralIterator, FastqPhredIterator


r1_file = '/mnt/ltian/simulated_data_for_fusion/HealthyDonor_107059_cfDNA_Fusion_R1.fastq.gz'
r2_file = '/mnt/ltian/simulated_data_for_fusion/HealthyDonor_107059_cfDNA_Fusion_R2.fastq.gz'
r1_removed_umi = r1_file + '_removed_umi.fastq'
r2_removed_umi = r2_file + '_removed_umi.fastq'
r1_umi_table = r1_file + '_umi_table.txt'
r2_umi_table = r2_file + '_umi_table.txt'
r1_modified = r1_file+'_{}_modified_{}.fastq'
r2_modified = r2_file+'_{}_modified_{}.fastq'
r1_umi_added = r1_modified + '_umi_added.fastq'
r2_umi_added = r2_modified + '_umi_added.fastq'


def write_fq_and_save_umi(id, seq, qual, fq_out, umi_out):
    umi = seq[:5]
    sequence = seq[5:]
    umi_score = qual[:5]
    score = qual[5:]
    record = "\n".join(['@'+id, sequence, "+", score])
    fq_out.write(record + "\n")
    umi_out.write("\n".join(['#',umi,'#',umi_score])+'\n')


def add_umi(r, umi_line, fout):
    if umi_line.strip() == '#':
        fout.write(r.strip() + '\n')
    else:
        fout.write(umi_line.strip() + r.strip() + '\n')


def remove_and_save_umi():
    with open(r1_removed_umi, 'w') as fout1, open(r2_removed_umi, 'w') as fout2, open(
            r1_umi_table, 'w') as fout3, open(r2_umi_table, 'w') as fout4:
        with gzip.open(r1_file, 'rt') as f1, gzip.open(r2_file, 'rt') as f2:
            for (id1, seq1, qual1), (id2, seq2, qual2) in zip(FastqGeneralIterator(f1), FastqGeneralIterator(f2)):
                write_fq_and_save_umi(id1, seq1, qual1, fout1, fout3)
                write_fq_and_save_umi(id2, seq2, qual2, fout2, fout4)


def add_umi_back(vaf, orientation):
    with open(r1_umi_added.format(orientation,vaf.strip("%")), 'w') as fout1, open(r2_umi_added.format(orientation,vaf.strip("%")), 'w') as fout2:
        with open(r1_modified.format(orientation,vaf.strip("%")), 'r') as f1, open(r2_modified.format(orientation,vaf.strip("%")), 'r') as f2, open(r1_umi_table, 'r') as r1_umi, open(r2_umi_table, 'r') as r2_umi:
            for r1, r2, r1_umi_line, r2_umi_line in zip(f1, f2, r1_umi, r2_umi):
                add_umi(r1, r1_umi_line, fout1)
                add_umi(r2, r2_umi_line, fout2)


if __name__ == '__main__':
    func = sys.argv[1]
    if func == "rm":
        remove_and_save_umi()
    if func == 'add':
        vaf = sys.argv[2]
        orientation = sys.argv[3]
        add_umi_back(vaf, orientation)

