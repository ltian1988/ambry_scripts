#!/bin/bash
#$ -j y
#$ -cwd
#$ -S /bin/bash
#$ -pe mpi 8
#$ -q test.q
#$ -M ltian@ambrygen.com
#$ -m beas


function showtime {
local T=$1
local D=$((T/60/60/24))
local H=$((T/60/60%24))
local M=$((T/60%60))
local S=$((T%60))
printf 'Running time: '
(( $D > 0 )) && printf '%d days ' $D
(( $H > 0 )) && printf '%d hours ' $H
(( $M > 0 )) && printf '%d minutes ' $M
(( $D > 0 || $H > 0 || $M > 0 )) && printf 'and '
printf '%d seconds\n' $S
}


source ~/.bash_profile
cd /mnt/ltian/simulated_data_for_fusion

date
start=`date +%s`
echo -e "===================\n"


python /mnt/ltian/ambry_scripts/Simulate_reads_for_fusion/simulate_reads.py /mnt/ltian/simulated_data_for_fusion/fusion_events_all_levels.txt a+b
# python /mnt/ltian/ambry_scripts/Simulate_reads_for_fusion/simulate_reads.py 2 a+b
# python /mnt/ltian/ambry_scripts/Simulate_reads_for_fusion/simulate_reads.py 5 a+b
# python /mnt/ltian/ambry_scripts/Simulate_reads_for_fusion/simulate_reads.py 0.1 a+b
# python /mnt/ltian/ambry_scripts/Simulate_reads_for_fusion/simulate_reads.py 0.2 a+b
# python /mnt/ltian/ambry_scripts/Simulate_reads_for_fusion/simulate_reads.py 0.5 a+b

cd /mnt/ltian/simulated_data_for_fusion/
export SENTIEON_LICENSE='usav1svsenp1.ambrygenetics.local:8990'
/mnt/zlei/projects/ctDNA/src/bin/sentieon-genomics-201911/bin/sentieon bwa mem -t 8 -K 10000000 -R "@RG\tID:HealthyDonor_107059\tSM:HealthyDonor_107059\tPL:ILLUMINA" /mnt/zlei/projects/ctDNA/src/BWAIndex/whole_genome.fa HealthyDonor_107059_cfDNA_Fusion_R1.fastq.gz_a+b_modified_1.fastq HealthyDonor_107059_cfDNA_Fusion_R2.fastq.gz_a+b_modified_1.fastq | /mnt/zlei/projects/ctDNA/src/bin/sentieon-genomics-201911/bin/sentieon util sort -i - -o HealthyDonor_107059_a+b_VAF1.bam --sam2bam
/mnt/zlei/projects/ctDNA/src/bin/sentieon-genomics-201911/bin/sentieon bwa mem -t 8 -K 10000000 -R "@RG\tID:HealthyDonor_107059\tSM:HealthyDonor_107059\tPL:ILLUMINA" /mnt/zlei/projects/ctDNA/src/BWAIndex/whole_genome.fa HealthyDonor_107059_cfDNA_Fusion_R1.fastq.gz_a+b_modified_2.fastq HealthyDonor_107059_cfDNA_Fusion_R2.fastq.gz_a+b_modified_2.fastq | /mnt/zlei/projects/ctDNA/src/bin/sentieon-genomics-201911/bin/sentieon util sort -i - -o HealthyDonor_107059_a+b_VAF2.bam --sam2bam
/mnt/zlei/projects/ctDNA/src/bin/sentieon-genomics-201911/bin/sentieon bwa mem -t 8 -K 10000000 -R "@RG\tID:HealthyDonor_107059\tSM:HealthyDonor_107059\tPL:ILLUMINA" /mnt/zlei/projects/ctDNA/src/BWAIndex/whole_genome.fa HealthyDonor_107059_cfDNA_Fusion_R1.fastq.gz_a+b_modified_5.fastq HealthyDonor_107059_cfDNA_Fusion_R2.fastq.gz_a+b_modified_5.fastq | /mnt/zlei/projects/ctDNA/src/bin/sentieon-genomics-201911/bin/sentieon util sort -i - -o HealthyDonor_107059_a+b_VAF5.bam --sam2bam
/mnt/zlei/projects/ctDNA/src/bin/sentieon-genomics-201911/bin/sentieon bwa mem -t 8 -K 10000000 -R "@RG\tID:HealthyDonor_107059\tSM:HealthyDonor_107059\tPL:ILLUMINA" /mnt/zlei/projects/ctDNA/src/BWAIndex/whole_genome.fa HealthyDonor_107059_cfDNA_Fusion_R1.fastq.gz_a+b_modified_0.1.fastq HealthyDonor_107059_cfDNA_Fusion_R2.fastq.gz_a+b_modified_0.1.fastq | /mnt/zlei/projects/ctDNA/src/bin/sentieon-genomics-201911/bin/sentieon util sort -i - -o HealthyDonor_107059_a+b_VAF0.1.bam --sam2bam
/mnt/zlei/projects/ctDNA/src/bin/sentieon-genomics-201911/bin/sentieon bwa mem -t 8 -K 10000000 -R "@RG\tID:HealthyDonor_107059\tSM:HealthyDonor_107059\tPL:ILLUMINA" /mnt/zlei/projects/ctDNA/src/BWAIndex/whole_genome.fa HealthyDonor_107059_cfDNA_Fusion_R1.fastq.gz_a+b_modified_0.2.fastq HealthyDonor_107059_cfDNA_Fusion_R2.fastq.gz_a+b_modified_0.2.fastq | /mnt/zlei/projects/ctDNA/src/bin/sentieon-genomics-201911/bin/sentieon util sort -i - -o HealthyDonor_107059_a+b_VAF0.2.bam --sam2bam
/mnt/zlei/projects/ctDNA/src/bin/sentieon-genomics-201911/bin/sentieon bwa mem -t 8 -K 10000000 -R "@RG\tID:HealthyDonor_107059\tSM:HealthyDonor_107059\tPL:ILLUMINA" /mnt/zlei/projects/ctDNA/src/BWAIndex/whole_genome.fa HealthyDonor_107059_cfDNA_Fusion_R1.fastq.gz_a+b_modified_0.5.fastq HealthyDonor_107059_cfDNA_Fusion_R2.fastq.gz_a+b_modified_0.5.fastq | /mnt/zlei/projects/ctDNA/src/bin/sentieon-genomics-201911/bin/sentieon util sort -i - -o HealthyDonor_107059_a+b_VAF0.5.bam --sam2bam

python /mnt/ltian/ambry_scripts/Simulate_reads_for_fusion/manipulate_umi.py add 1 a+b
python /mnt/ltian/ambry_scripts/Simulate_reads_for_fusion/manipulate_umi.py add 2 a+b
python /mnt/ltian/ambry_scripts/Simulate_reads_for_fusion/manipulate_umi.py add 5 a+b
python /mnt/ltian/ambry_scripts/Simulate_reads_for_fusion/manipulate_umi.py add 0.1 a+b
python /mnt/ltian/ambry_scripts/Simulate_reads_for_fusion/manipulate_umi.py add 0.2 a+b
python /mnt/ltian/ambry_scripts/Simulate_reads_for_fusion/manipulate_umi.py add 0.5 a+b

echo -e "\n==================="
date
end=`date +%s`
t=$((end-start))
showtime $t
