import pysam
import random
from Bio import SeqIO
from Bio.Seq import Seq
import gzip
from pyfaidx import Fasta
import requests
import pandas as pd
import sys

BAM_FILE = "HealthyDonor_107059.bam"
bam = pysam.AlignmentFile(BAM_FILE, 'rb')
qc = 0
vafs = ['0.1%','1%','2%','5%','0.2%','0.5%']
r1_file = '/mnt/ltian/simulated_data_for_fusion/HealthyDonor_107059_cfDNA_Fusion_R1.fastq.gz'
r2_file = '/mnt/ltian/simulated_data_for_fusion/HealthyDonor_107059_cfDNA_Fusion_R2.fastq.gz'
r1_removed_umi = r1_file + '_removed_umi.fastq'
r2_removed_umi = r2_file + '_removed_umi.fastq'
r1_modified = r1_file+'_{}_modified_{}.fastq'
r2_modified = r2_file+'_{}_modified_{}.fastq'
hg19 = '/mnt/zlei/projects/ctDNA/src/BWAIndex/whole_genome.fa'


ref_genome = Fasta(hg19)


def query_strand(gene):
    server = "https://grch37.rest.ensembl.org"
    ext = f"/lookup/symbol/homo_sapiens/{gene}?expand=1"
    r = requests.get(server + ext, headers={"Content-Type": "application/json"})
    decoded = r.json()
    if decoded['strand'] == -1:
        strand = '-'
    else:
        strand = '+'
    return strand


def list_sequence_id_by_location(chr, pos, qc, vaf, gene_a):
    strand_a = query_strand(gene_a)
    depth = max(bam.count_coverage(chr, pos-1, pos, quality_threshold=qc))[0]
    if depth < 5000:
        return []
    supporting_read_num = round(depth * float(vaf.strip('%')) / 100)
    pool = []
    pool_append = pool.append
    for read in bam.fetch(chr, pos-1, pos):
        record = str(read).split('\t')
        pool_append(record)
    if strand_a == '+':
        target_start = [pos-120, pos-90]
    else:
        target_start = [pos-55, pos-25]
    candidate = [i for i in pool if target_start[0] < int(i[3]) < target_start[1] and len(i[9])==146]
    selected_records = random.sample(candidate, supporting_read_num)
    return selected_records


def modify_reads(selected_records, gene_b, chr_b, pos_b, strand_a, pos_a, orientation):
    strand_b = query_strand(gene_b)
    modified_reads = {}
    for record in selected_records:
        read_start = int(record[3])
        raw_read = record[9]
        if strand_a == '+':
            soft_clipping_length = 146 - (pos_a - read_start) - 1
            clipped_seq = raw_read[:pos_a-read_start]
            if strand_b == '+':
                soft_clipping_seq = str(ref_genome[chr_b][pos_b - 1:pos_b + soft_clipping_length])
            else:
                soft_clipping_seq = str(ref_genome[chr_b][pos_b - soft_clipping_length - 1:pos_b].complement)[::-1]
            new_read = clipped_seq + soft_clipping_seq
        else:
            soft_clipping_length = pos_a - read_start + 1
            if orientation == 'b+a':
                clipped_seq = raw_read[soft_clipping_length:]
                if strand_b == '+':
                    soft_clipping_seq = str(ref_genome[chr_b][pos_b - soft_clipping_length + 1:pos_b + 1].complement)[::-1]
                else:
                    soft_clipping_seq = str(ref_genome[chr_b][pos_b - 1:pos_b + soft_clipping_length - 1])
                new_read = soft_clipping_seq + clipped_seq
            else:
                clipped_seq = str(Seq(raw_read[soft_clipping_length:]).reverse_complement())
                if strand_b == '+':
                    soft_clipping_seq = str(ref_genome[chr_b][pos_b - 1:pos_b + soft_clipping_length - 1])
                else:
                    soft_clipping_seq = str(ref_genome[chr_b][pos_b - soft_clipping_length + 1:pos_b + 1].complement)[
                                        ::-1]
                new_read = clipped_seq + soft_clipping_seq
        print(record[0])
        print(f"strandAB:{strand_a}/{strand_b}")
        print(raw_read)
        print(new_read)
        assert len(new_read) == 146
        modified_reads[record[0]] = new_read
    return modified_reads


def modify_fastq(selected_records, modified_reads, vaf, orientation):
    selected_reads = {i[0]:i for i in selected_records}
    with open(r1_modified.format(orientation,vaf.strip("%")), 'w') as fout1, open(r2_modified.format(orientation,vaf.strip("%")), 'w') as fout2:
        with open(r1_removed_umi, 'r') as f1, open(r2_removed_umi, 'r') as f2:
            for records in zip(SeqIO.parse(f1, "fastq"), SeqIO.parse(f2, "fastq")):
                record = records[0]
                record2 = records[1]
                if record.id in selected_reads:
                    if str(record.seq) == selected_reads[record.id][9]:
                        new_record = record
                        new_record.seq = Seq(modified_reads[record.id])
                        SeqIO.write(new_record, fout1, 'fastq')
                        SeqIO.write(record2, fout2, 'fastq')
                        continue
                    if str(record.seq.reverse_complement()) == selected_reads[record.id][9]:
                        new_record = record
                        new_record.seq = Seq(modified_reads[record.id]).reverse_complement()
                        SeqIO.write(new_record, fout1, 'fastq')
                        SeqIO.write(record2, fout2, 'fastq')
                        continue
                    if str(record2.seq) == selected_reads[record.id][9]:
                        new_record = record2
                        new_record.seq = Seq(modified_reads[record.id])
                        SeqIO.write(new_record, fout2, 'fastq')
                        SeqIO.write(record, fout1, 'fastq')
                        continue
                    if str(record2.seq.reverse_complement()) == selected_reads[record.id][9]:
                        new_record = record2
                        new_record.seq = Seq(modified_reads[record2.id]).reverse_complement()
                        SeqIO.write(new_record, fout2, 'fastq')
                        SeqIO.write(record, fout1, 'fastq')
                        continue
                else:
                    SeqIO.write(record, fout1, 'fastq')
                    SeqIO.write(record2, fout2, 'fastq')



if __name__ == '__main__':
    # chimerdb = pd.read_csv('chimerdb.csv', sep=',', header=0, index_col=None)
    # gene_a_list = [
    #     "ALK",
    #     "FGFR1",
    #     "FGFR2",
    #     "FGFR3",
    #     "NTRK1",
    #     "NTRK2",
    #     "NTRK3",
    #     "RET",
    #     "ROS1"
    # ]
    fusion_table = sys.argv[1]
    fusion_ref = pd.read_csv(fusion_table, sep='\t', header=0, index_col=None)
    orientation = str(sys.argv[2])
    # reference_table = open(f"fusion_events_design_{orientation}_VAF{vaf}.txt", 'w')
    # reference_table.write("gene_A\tchr_pos_A\tgene_B\tchr_pos_B\tstrand\tVAF\n")
    # vaf = vaf+'%'
    all_selected_records = []
    all_modified_reads = {}
    for vaf in vafs:
        for i in fusion_ref.index:
            gene_a = fusion_ref.loc[i, "gene_A"]
            [strand_a, strand_b] = fusion_ref.loc[i, "strand"].split('/')
            gene_b = fusion_ref.loc[i, "gene_B"]
            [chr_a, pos_a] = fusion_ref.loc[i, "chr_pos_A"].split('_')
            pos_a = int(pos_a)
            [chr_b, pos_b] = fusion_ref.loc[i, "chr_pos_B"].split('_')
            pos_b = int(pos_b)
            selected_records = list_sequence_id_by_location(chr_a,
                                                            pos_a,
                                                            qc,
                                                            vaf,
                                                            gene_a)
            all_selected_records = all_selected_records + selected_records
            modified_reads = modify_reads(selected_records,gene_b,chr_b,pos_b,strand_a,pos_a, orientation)
            all_modified_reads.update(modified_reads)
        modify_fastq(all_selected_records, all_modified_reads, vaf, orientation)


    # for gene_a in gene_a_list:
    #     strand_a = query_strand(gene_a)
    #     sub_df = chimerdb[chimerdb['gene'] == gene_a]
    #     idx = list(sub_df.index)
    #     random.shuffle(idx)
    #     for i in idx:
    #         random_row = sub_df.loc[i, ]
    #         gene_b = random_row['geneAB'].split('_')[0]
    #         if gene_a == gene_b:
    #             continue
    #         [gene_b_chrpos, gene_a_chrpos] = random_row['Full_junction'].split('__')
    #         chr_a = gene_a_chrpos.split('_')[0]
    #         pos_a = int(gene_a_chrpos.split('_')[1])
    #         selected_records = list_sequence_id_by_location(chr_a,
    #                                                         pos_a,
    #                                                         qc,
    #                                                         vaf,
    #                                                         gene_a)
    #         try:
    #             strand_b = query_strand(gene_b)
    #         except KeyError:
    #             selected_records = []
    #         if selected_records:
    #             break
    #     if selected_records:
    #         all_selected_records = all_selected_records + selected_records
    #         chr_b = gene_b_chrpos.split('_')[0]
    #         pos_b = int(gene_b_chrpos.split('_')[1])
    #         reference_table.write(f"{gene_a}\t{chr_a}_{pos_a}\t{gene_b}\t{chr_b}_{pos_b}\t{strand_a}/{strand_b}\t{vaf}\n")
    #         modified_reads = modify_reads(selected_records,gene_b,chr_b,pos_b,strand_a,pos_a, orientation)
    #         all_modified_reads.update(modified_reads)
    # modify_fastq(all_selected_records, all_modified_reads,vaf, orientation)






